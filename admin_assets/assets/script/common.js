$(document).ready(function(){
    $('input:checkbox').click(function() {
        $('input:checkbox').not(this).prop('checked', false);
    });
    $('#user_id_multi').select2();
    $(function() {
        $('input[name="dob"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: true,
            startDate:'01/01/1991',
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    });
    $('input[name="dob"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY')).trigger('change');
    });
    $(function() {
        $('input[name="date"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    });
    $('#number').mask('0000-0000-0000',{clearIfNotMatch: true});
    $('#pan_no').mask('SSSSS0000S',{clearIfNotMatch: true});
    (function($) {
        $.fn.inputFilter = function(inputFilter) {
          return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
              this.oldValue = this.value;
              this.oldSelectionStart = this.selectionStart;
              this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
              this.value = this.oldValue;
              this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
              this.value = "";
            }
          });
        };
      }(jQuery));
      $(".number").inputFilter(function(value) {
        return /^-?\d*$/.test(value); });
  });
var SERVER_ERROR_MSG = 'Something Went Wrong! Please Try Again';
function show_success(message){
    if($.notify){
        $.notify({
            message: message
        },{
            type: 'success'
        });
    }
    $('#formMessage').addClass('alert alert-success').html(message);
}
function show_error(message){
    if($.notify){
        $.notify({
            message: message,
        },{
            type: 'error'
        });
    }
    $('#formMessage').addClass('alert alert-danger').html(message);
}
function show_warning(message){
    if($.notify){
        $.notify({
            message: message
        },{
            type: 'warning'
        });
    }
}



$('#ChangePasswordForm').validate({
    rules: {
        old_password: {
            required: true,
            remote: {
                url: base_url + 'admin/dashboard/check_password',
                type: 'post',
                data: {
                    password: function () {
                        return $('#old_password').val();
                    }
                }
            }
        },
        new_password: {
            required: true,
            minlength: 6,
        },
        confirm_new_password: {
            required: true,
            equalTo: "#new_password"
        },
    },
    submitHandler: function (form) {
        var url = base_url + 'admin/dashboard/do_change_password';
        $.ajax({
            url: url,
            type: form.method,
            data: $(form).serialize(),
            success: function (response) {
                response_data = response;
                if (response_data.code == 100) {
                    document.getElementById('ChangePasswordForm').reset();
                    swal("Success!", response.message, "success").then((value) => {
                        window.location.href = base_url + 'admin/dashboard';
                    });
                } else if (response_data.code == 101) {
                    swal("Success!", response.message, "error");
                } else {
                    swal("Success!", 'Some error occurred at server side.', "error");
                }
            }
        });
    }
});



