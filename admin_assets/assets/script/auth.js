$("#loginForm").validate({
    rules: {
        email: {
            required: true,
            email: true,
            maxlength: 100
        },
        password: {
            required: true,
            maxlength: 100
        },
    },
    submitHandler: function(form) {
        $('#formMessage').removeClass('alert alert-danger alert-success').html("");
        $('#btn-login').attr('disabled', 'disabled');
        $.ajax({
            url: siteUrl + 'admin/auth/doLogin',
            type: form.method,
            data: $(form).serialize(),
            success: function(response) {
                if(response.code == 100) {
                    $('#formMessage').addClass('alert alert-success').html(response.message);
                    window.location = siteUrl + 'admin/';
                } else if (response.code == 101) {
                    $('#formMessage').addClass('alert alert-danger').html(response.message);
                } else {
                    $('#formMessage').addClass('alert alert-danger').html(SERVER_ERROR_MSG);
                }
                $('#btn-login').attr('disabled', false);
            }
        });
    }
});
