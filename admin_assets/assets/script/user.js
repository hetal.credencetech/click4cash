$(document).ready(function () {
    if($('#kyc_unapproved_request').length > 0){
        
        // var remarkEditor = null;
        // ClassicEditor.create(document.querySelector('#kyc_unapproved_request #remark')).then( editor => {
        //     remarkEditor = editor;
        // }).catch( error => {
        //     console.error( error );
        // });
        show_kyc_details(0,1);
    }
});    

var userDataTable=null,userEnquiryDataTable=null,transactionDataTable=null,pendingTransactionDataTable=null;
if($('#userDataTable').length > 0){
    userDataTable = $('#userDataTable').DataTable({
        "aaSorting": [[2,'desc']],
        "bAutoWidth": false,
        "pagingType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "pageLength": 50,
        "ajax": {
            "url" : site_url + "admin/user/ajax_user_list",
            "type" : 'POST',
        },
        columns: [
            {
                data: "phoneNumber",title:"Phone number"
            },
            {
                data: "isVerified",title:"Is Verified",
                render: function (data, type, row) {
                    var content = '';
                    if(data == 0) {
                        content = '<a class="badge bg-danger">No</a>';
                    } else  if(data == 1) {
                        content = '<a class="badge bg-success confirm">Yes</a>';
                    } 
                    return content;  
                }
            },
            {
                data: "userID",title:"Action",
                orderable : false,
                render: function (data, type, row) {
                    var content = '<a href="javascript:;" title="View Record" class="btn btn-info view-detail action"><i class="icon-eye"></i></a> ';
                    //content += '<a href="' + base_url + 'admin/user/edit/' + data + '" title="Edit Record" class="btn btn-info action"><i class="icon-pencil5"></i></a> ';
                    content += '<a href="javascript:;" title="Delete Record" class="btn btn-danger delete-record action" item-id="' + data + '"><i class="icon-trash-alt"></i></a> ';
                    return content;
                }
            },
        ],
    });
    $(document).on('click', '.delete-record', function () {
        var data_id = $(this).attr('item-id').trim();
        var tableID = $(this).closest('table').attr('id');
        var url = site_url + 'admin/user/delete_user/' + data_id;
        if (data_id != null && data_id != '') {
            swal({
                title: "Delete Record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then((confirm) => {
                if (confirm.value) {
                $.post(url, function (response) {
                    if (response.code == 100) {
                        userDataTable.ajax.reload();
                        show_success(response.message);
                    } else if (response.code == 101) {
                        show_error(response.message);
                    } else {
                        show_error("Some error occurred at server side!");
                    }
                });
            }
        });
    }
    });

}


if($('#userEnquiryDataTable').length > 0){
    userEnquiryDataTable = $('#userEnquiryDataTable').DataTable({
        "aaSorting": [[0,'desc']],
        "bAutoWidth": false,
        "pagingType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "pageLength": 50,
        "ajax": {
            "url" : site_url + "admin/user/ajax_user_enquiry_list",
            "type" : 'POST',
        },
        columns: [
            {
                data: "userenquiryID",title:"User Enquiry ID"
            },
            {
                data: "phoneNumber",title:"Phone number"
            },
            {
                data: "attachment",title:"Attachment",
                render: function (data, type, row) {
                    var content = '<a download href="' +data + '" title="">Click Here</a>';
                    return content;
                }
            },
            {
                data: "userenquiryID",title:"Action",
                orderable : false,
                render: function (data, type, row) {
                    var content = '<a href="javascript:;" title="View Record" class="btn btn-info view-detail action"><i class="icon-eye"></i></a> ';
                    //content += '<a href="' + base_url + 'admin/user/edit/' + data + '" title="Edit Record" class="btn btn-info action"><i class="icon-pencil5"></i></a> ';
                    content += '<a href="javascript:;" title="Delete Record" class="btn btn-danger delete-record action" item-id="' + data + '"><i class="icon-trash-alt"></i></a> ';
                    return content;
                }
            },
        ],
    });
    $(document).on('click', '.delete-record', function () {
        var data_id = $(this).attr('item-id').trim();
        var tableID = $(this).closest('table').attr('id');
        var url = site_url + 'admin/user/delete_user_enquiry/' + data_id;
        if (data_id != null && data_id != '') {
            swal({
                title: "Delete Record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then((confirm) => {
                if (confirm.value) {
                $.post(url, function (response) {
                    if (response.code == 100) {
                        userEnquiryDataTable.ajax.reload();
                        show_success(response.message);
                    } else if (response.code == 101) {
                        show_error(response.message);
                    } else {
                        show_error("Some error occurred at server side!");
                    }
                });
            }
        });
    }
    });

}
if($('#transactionDataTable').length > 0){
    transactionDataTable = $('#transactionDataTable').DataTable({
        "aaSorting": [[2,'desc']],
        "bAutoWidth": false,
        "pagingType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "pageLength": 50,
        "ajax": {
            "url" : site_url + "admin/user/ajax_transaction_list",
            "type" : 'POST',
        },
        columns: [
            {
                data: "phoneNumber",title:"Phone number"
            },
            {
                data: "bankName",title:"Bank Name"
            },
            {
                data: "accountName",title:"Account Name"
            },
            {
                data: "accountNumber",title:"Account Number"
            },
            {
                data: "amount",title:"Amount"
            },
            {
                data: "status",title:"Status",
                render: function (data, type, row) {
                    var content = '';
                    if(data == 0) {
                        content = '<a class="badge bg-info">Pending</a>';
                    } else  if(data == 1) {
                        content = '<a class="badge bg-success confirm">Completed</a>';
                    } else  if(data == 2) {
                        content = '<a class="badge bg-danger">Failed</a>';
                    }  
                    return content;  
                }
            },
            {
                data: "transactionID",title:"Action",
                orderable : false,
                render: function (data, type, row) {
                    var content = '<a href="javascript:;" title="View Record" class="btn btn-info view-detail action"><i class="icon-eye"></i></a> ';
                    //content += '<a href="' + base_url + 'admin/user/edit/' + data + '" title="Edit Record" class="btn btn-info action"><i class="icon-pencil5"></i></a> ';
                    content += '<a href="javascript:;" title="Delete Record" class="btn btn-danger delete-record action" item-id="' + data + '"><i class="icon-trash-alt"></i></a> ';
                    return content;
                }
            },
        ],
    });
    $(document).on('click', '.delete-record', function () {
        var data_id = $(this).attr('item-id').trim();
        var tableID = $(this).closest('table').attr('id');
        var url = site_url + 'admin/user/delete_transaction/' + data_id;
        if (data_id != null && data_id != '') {
            swal({
                title: "Delete Record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then((confirm) => {
                if (confirm.value) {
                $.post(url, function (response) {
                    if (response.code == 100) {
                        transactionDataTable.ajax.reload();
                        show_success(response.message);
                    } else if (response.code == 101) {
                        show_error(response.message);
                    } else {
                        show_error("Some error occurred at server side!");
                    }
                });
            }
        });
    }
    });

}

if($('#pendingTransactionDataTable').length > 0){
    pendingTransactionDataTable = $('#pendingTransactionDataTable').DataTable({
        "aaSorting": [[2,'desc']],
        "bAutoWidth": false,
        "pagingType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "pageLength": 50,
        "ajax": {
            "url" : site_url + "admin/user/ajax_pending_transaction_list",
            "type" : 'POST',
        },
        columns: [
            {
                data: "phoneNumber",title:"Phone number"
            },
            {
                data: "bankName",title:"Bank Name"
            },
            {
                data: "accountName",title:"Account Name"
            },
            {
                data: "accountNumber",title:"Account Number"
            },
            {
                data: "amount",title:"Amount"
            },
            {
                data: "status",title:"Status",
                render: function (data, type, row) {
                    var content = '';
                    if(data == 0) {
                        content = '<a class="badge bg-info">Pending</a>';
                    } else  if(data == 1) {
                        content = '<a class="badge bg-success confirm">Completed</a>';
                    } else  if(data == 2) {
                        content = '<a class="badge bg-danger">Failed</a>';
                    }
                    return content;
                }
            },
            {
                data: "transactionID",title:"Action",
                orderable : false,
                render: function (data, type, row) {
                    var content = '<a href="javascript:;" title="View Record" class="btn btn-info view-detail action"><i class="icon-eye"></i></a> ';
                    //content += '<a href="' + base_url + 'admin/user/edit/' + data + '" title="Edit Record" class="btn btn-info action"><i class="icon-pencil5"></i></a> ';
                    content += '<a href="javascript:;" title="Delete Record" class="btn btn-danger delete-record action" item-id="' + data + '"><i class="icon-trash-alt"></i></a> ';
                    if(row.status == 0) {
                        content = content + '<a href="javascript:;" title="Change Status" class="btn btn-primary edit-trnasaction-status-detail">Change Status</a> '
                    } 
                    else  if(row.status == 3) {
                        content = content + '<a href="javascript:;" title="Change Status" class="btn btn-primary edit-trnasaction-remark-detail" style="display:none">Change Remark</a> '
                    }
                    else {
                        content = content + '<a href="javascript:;" title="Change Status" class="btn btn-primary edit-trnasaction-remark-detail">Change Remark</a> '
                    }         
                    return content;
                }
            },
        ],
    });
    $(document).on('click', '.delete-record', function () {
        var data_id = $(this).attr('item-id').trim();
        var tableID = $(this).closest('table').attr('id');
        var url = site_url + 'admin/user/delete_transaction/' + data_id;
        if (data_id != null && data_id != '') {
            swal({
                title: "Delete Record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then((confirm) => {
                if (confirm.value) {
                $.post(url, function (response) {
                    if (response.code == 100) {
                        pendingTransactionDataTable.ajax.reload();
                        show_success(response.message);
                    } else if (response.code == 101) {
                        show_error(response.message);
                    } else {
                        show_error("Some error occurred at server side!");
                    }
                });
            }
        });
    }
    });

    var remarkEditor = null;
    ClassicEditor.create(document.querySelector('#remark')).then( editor => {
        remarkEditor = editor;
    }).catch( error => {
        console.error( error );
    });


    $(document).on('click', '.edit-trnasaction-status-detail', function(e) {
        var RowData = pendingTransactionDataTable.row($(this).closest('tr')).data();
        console.log(RowData);
        $('#transactionID').val(RowData['transactionID']);
        $('input[name="status"][value="' + RowData['status'] + '"]').attr('checked', true);
        if(RowData['remark'] == null){
            RowData['remark']="";
        }
        remarkEditor.data.set(RowData['remark']);
        $("#TransactionStatusRemarkModal").modal();
    });

}

if($('#kycDataTable').length > 0){
    kycDataTable = $('#kycDataTable').DataTable({
        "aaSorting": [[2,'desc']],
        "bAutoWidth": false,
        "pagingType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "pageLength": 50,
        "ajax": {
            "url" : site_url + "admin/user/ajax_kyc_list",
            "type" : 'POST',
        },
        columns: [
            {
                data: "phoneNumber",title:"Phone number"
            },
            {
                data: "aadharCardFrontImage",title:"Aadhar Front Image",
                render: function (data, type, row) {
                    var content = '<a class="fancybox" rel="gallery1" href="' + site_url+"uploads/"+data + '" title=""><img src="'+site_url+"uploads/"+data+'" width="75px" height="75px"></a>';
                    return content;
                }
            },
            {
                data: "aadharCardBackImage",title:"Aadhar Back Image",
                render: function (data, type, row) {
                    var content = '<a class="fancybox" rel="gallery1" href="' + site_url+"uploads/"+data + '" title=""><img src="'+site_url+"uploads/"+data+'" width="75px" height="75px"></a>';
                    return content;
                }
            },
            {
                data: "userImage",title:"User Image",
                render: function (data, type, row) {
                    var content = '<a class="fancybox" rel="gallery1" href="' + site_url+"uploads/"+data + '" title=""><img src="'+site_url+"uploads/"+data+'" width="75px" height="75px"></a>';
                    return content;
                }
            },
            {
                data: "status",title:"Status",
                render: function (data, type, row) {
                    var content = '';
                    if(data == 'Unapproved') {
                        content = '<a class="badge bg-danger">Unapproved</a>';
                    } else  if(data == 'Approved') {
                        content = '<a class="badge bg-success confirm">Approved</a>';
                    }
                    return content;
                }
            },
            {
                data: "kycID",title:"Action",
                orderable : false,
                render: function (data, type, row) {
                    var content = '<a href="javascript:;" title="View Record" class="btn btn-info view-detail action"><i class="icon-eye"></i></a> ';
                        content += '<a href="javascript:;" title="Delete Record" class="btn btn-danger delete-record action" item-id="' + data + '"><i class="icon-trash-alt"></i></a> ';
                        content = content + '<a href="javascript:;" title="Change Status" class="btn btn-primary edit-kyc-status-detail">Change Status</a> '
                             
                    return content;
                }
            },
        ],
    });
    $(document).on('click', '.delete-record', function () {
        var data_id = $(this).attr('item-id').trim();
        var tableID = $(this).closest('table').attr('id');
        var url = site_url + 'admin/user/delete_transaction/' + data_id;
        if (data_id != null && data_id != '') {
            swal({
                title: "Delete Record?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then((confirm) => {
                if (confirm.value) {
                $.post(url, function (response) {
                    if (response.code == 100) {
                        pendingTransactionDataTable.ajax.reload();
                        show_success(response.message);
                    } else if (response.code == 101) {
                        show_error(response.message);
                    } else {
                        show_error("Some error occurred at server side!");
                    }
                });
            }
        });
    }
    });

    var remarkEditor = null;
    ClassicEditor.create(document.querySelector('#remark')).then( editor => {
        remarkEditor = editor;
    }).catch( error => {
        console.error( error );
    });


    $(document).on('click', '.edit-kyc-status-detail', function(e) {
        var RowData = kycDataTable.row($(this).closest('tr')).data();
        $('#kycID').val(RowData['kycID']);
        if(RowData['status'] == 'Unapproved')
        $('input[name="status"][value="0"]').attr('checked', true);
        else if(RowData['status'] == 'Approved')
        $('input[name="status"][value="1"]').attr('checked', true);
        if(RowData['remark'] == null){
            RowData['remark']="";
        }
        remarkEditor.data.set(RowData['remark']);
        $("#KycStatusRemarkModal").modal();
    });

}



$(document).on('click', '.view-detail', function (e) {
    var tableID = $(this).closest('table').attr('id');

    if (tableID == "userDataTable")
        var RowData = userDataTable.row($(this).closest('tr')).data();
    else if (tableID == "transactionDataTable")
    var RowData = transactionDataTable.row($(this).closest('tr')).data();  
    else if (tableID == "pendingTransactionDataTable")
    var RowData = pendingTransactionDataTable.row($(this).closest('tr')).data();
    else if (tableID == "kycDataTable")
    var RowData = kycDataTable.row($(this).closest('tr')).data(); 
    else if (tableID == "userEnquiryDataTable")
    var RowData = userEnquiryDataTable.row($(this).closest('tr')).data();  
    
    
    
    var RowDataKeys = Object.keys(RowData);
    $("#viewDataTableModal").find('.DataBody').empty();
    //console.log($("#"+tableID).parents('body').closest("#"+tableID+"Modal").find('.DataBody'));return false;
    $.each(RowDataKeys, function (index, row) {

        if(typeof RowData[row] == 'object' && RowData[row] != null)
        {
            // var subTable = '';
           
            // $.each(RowData[row], function (index1, row1) {
            //     subTable +='<tr>';
            //     subTable += '<td >' + row1.CreatedOn+'</td>';
            //     subTable += '</tr>';
            // });
           
            // $("#viewDataTableModal").find('.SubTableDataBody').html(subTable);
            // $('#subTable').css('display','block');
        }
        else
        {
         $("#viewDataTableModal").find('.DataBody').append('<tr><th style="width:20%;">' + insertSpaces(row) + '</th><td style="width:80%;">' + RowData[row] + '</td></tr>');
        }
    });
    $("#viewDataTableModal").modal();
});
function insertSpaces(string) {
    string = string.replace(/([a-z])([A-Z])/g, '$1 $2');
    string = string.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2');
    string = string.toLowerCase().replace(/\b[a-z]/g, function (letter) {
        return letter.toUpperCase();
    });

    return string;
}


$('#TransactionStatusRemarkForm').validate({
    rules: {
        status: {
            required: true,
        }
    },
    submitHandler: function(form){
        var formData= new FormData(form);
        $.ajax({
         url: site_url + "admin/user/update_status_transaction",
         type:'POST',
         dataType:'json',
         data: formData,
         success: function(response){
            if(response.code == 100) {
                pendingTransactionDataTable.ajax.reload();
                $('#TransactionStatusRemarkModal').modal('hide');
                show_success(response.message);
            } else if(response.code == 101) {
                show_error(response.message);
            } else {
                show_error('Some error occurred at server side!','error');
            }
         },
         cache: false,
         contentType: false,
         processData: false
        });
    }
});

$('#KycStatusRemarkForm').validate({
    rules: {
        status: {
            required: true,
        }
    },
    submitHandler: function(form){
        var formData= new FormData(form);
        $.ajax({
         url: site_url + "admin/user/update_status_kyc",
         type:'POST',
         dataType:'json',
         data: formData,
         success: function(response){
            if(response.code == 100) {
                kycDataTable.ajax.reload();
                $('#KycStatusRemarkModal').modal('hide');
                show_success(response.message);
            } else if(response.code == 101) {
                show_error(response.message);
            } else {
                show_error('Some error occurred at server side!','error');
            }
         },
         cache: false,
         contentType: false,
         processData: false
        });
    }
});


$(document).on('click','#kycDataTable .fancybox',function(e){
    $(this).parent('td').parent('tr').find('.fancybox').fancybox({
        padding: 3,
        iframe: {
            css: {
                height: '1000px',
            },
            preload: false
        }
    });
    return false;
});


$(document).on('click','#UnapprovedKycStatusRemarkForm .fancybox',function(e){
    $('.fancybox').fancybox({
        padding: 3,
        iframe: {
            css: {
                height: '1000px',
            },
            preload: false
        }
    });
    return false;
});


$('#UnapprovedKycStatusRemarkForm').validate({
    rules: {
        status: {
            required: true,
        }
    },
    submitHandler: function(form){

        var formData= new FormData(form);
        $.ajax({
         url: site_url + "admin/user/update_status_kyc",
         type:'POST',
         dataType:'json',
         data: formData,
         success: function(response){
            
            if(response.code == 100) {
                show_success(response.message);
                var button_value = $('#kyc_unapproved_request #save_next').attr('value');
                //alert(button_value);
                if(button_value == 'Save'){
                    return false;
                }
                    var status = $('#kyc_unapproved_request input[name="status"]:checked').val();
                    var start = $('#kyc_unapproved_request #start').val();
                    console.log(status);
                    
                    if(status  == 0){
                        start = Number(start) + Number(1);
                    }
                    show_kyc_details(start,1);
                
            } else if(response.code == 101) {
                show_error(response.message);
            } else {
                show_error('Some error occurred at server side!','error');
            }
         },
         cache: false,
         contentType: false,
         processData: false
        });
    }
});


$(document).on('click','#prev',function(e){
    var status = $('#kyc_unapproved_request input[name="status"]:checked').val();
    var start = $('#kyc_unapproved_request #start').val();
    console.log(status);
    
    start = Number(start) - Number(1);
    show_kyc_details(start,1);
    
})
function show_kyc_details(start,length){
    
    $('#kyc_unapproved_request #start').val(start);
   // console.log(start);
    $.ajax({
        type: "POST",
        url: site_url + "admin/user/ajax_kyc_list",
        dataType: "json",
        data: {orderBy: 'kycID',sortBy:'DESC',search:'status = 0',start:start,length:1},
        headers: {"access_token": localStorage.getItem("access_token")},
        success: function (data) {
           // console.log(data);return false;
            if (data.recordsFiltered > 0 )
            {
                $("#UnapprovedKycStatusRemarkForm")[0].reset();
                if(data.recordsFiltered == (Number(start) + Number(1))){
                    var save_next = "Save";
                }else{
                    var save_next = "Save And Next";
                }
                if(start == 0){
                    $('#kyc_unapproved_request #prev').css('display','none');
                }
                else
                $('#kyc_unapproved_request #prev').css('display','block');
                $('#kyc_unapproved_request #save_next').attr('value', save_next);
                
                $.each(data.data[0], function (key, value) {
                   
                    if (key == 'phoneNumber')
                    {
                        $('#kyc_unapproved_request input[name="phoneNumber"]').val(value);
                    } 
                    
                    if (key == 'kycID')
                    {
                        $('#kyc_unapproved_request input[name="kycID"]').val(value);
                    } 

                    if (key == 'aadharCardFrontImage')
                    {
                        var html=' <a class="fancybox" rel="gallery1" href="'+site_url+'uploads/'+value+'" title=""><img src="'+site_url+'uploads/'+value+'" width="150px" height="100px"></a>';
                        $('#kyc_unapproved_request #aadharCardFrontImage').html(html);
                    } 

                    if (key == 'aadharCardBackImage')
                    {
                        var html=' <a class="fancybox" rel="gallery1" href="'+site_url+'uploads/'+value+'" title=""><img src="'+site_url+'uploads/'+value+'" width="150px" height="100px"></a>';
                        $('#kyc_unapproved_request #aadharCardBackImage').html(html);
                    } 

                    if (key == 'userImage')
                    {
                        var html=' <a class="fancybox" rel="gallery1" href="'+site_url+'uploads/'+value+'" title=""><img src="'+site_url+'uploads/'+value+'" width="150px" height="100px"></a>';
                        $('#kyc_unapproved_request #userImage').html(html);
                    } 

                    

                    if (key == 'kycID')
                    {
                        $('#kyc_unapproved_request input[name="kycID"]').val(value);
                    } 

                    if (key == 'status')
                    {
                        if(value == "Approved")
                        value = 1;
                        else
                        value = 0;
                        $('#kyc_unapproved_request input[name="status"][value="' + value + '"]').attr('checked', true);
  
                    } 

                    if (key == 'remarks')
                    {
                        if(value != "" && value != null)
                         $('#kyc_unapproved_request #remark').html(value);
                    } 
                });
            }else{
                $('#kyc_unapproved_request').css('display','none');
                $('#FormMessage').addClass('alert alert-danger').html('Not any unapproved kyc user.');
            }
        },
        error: function () {

        }
    });
}


