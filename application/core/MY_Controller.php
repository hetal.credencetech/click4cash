<?php
//use PHPMailer\PHPMailer\PHPMailer;
//use PHPMailer\PHPMailer\Exception;

/**
 * Extend codeigniter core classe to expand functionality
 *
 * @author suresh saini
 */
class My_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->store_logs();
    }

    /**
     * Set output response with header content type
     *
     * @param mixed $response_array
     * @param string $content_type
     * @param boolean $is_json_data
     * @param boolean $is_log
     * @return void
     */
    public function set_response($response_array, $content_type = 'application/json' , $is_json_data = true) {
        $data = $response_array;
        if($is_json_data == true) {
            $data = json_encode($data);
        }
        $this->output->set_content_type($content_type)->set_output($data);
    }

    /**
     * Set output response with header content type
     *
     * @param mixed $response_array
     * @param string $content_type
     * @param boolean $is_json_data
     * @param boolean $is_log
     * @return void
     */
    public function set_error_response($message = ERROR_MESSAGE) {

        $data = array(
            "code" => StatusCodes::FAILURE,
            "message" => $message
        );
        $data = json_encode($data);
        $this->output->set_content_type('application/json')->set_output($data);
    }
    /**
     *
     * @param string $to email to
     * @param string $subject email subject
     * @param string $message email message
     * @param array $attachments email attachments
     * @return boolean
     */
    public function send_mail($to, $subject, $message, $attachments = null) {
        // $send_system_email_url = base_url('site/send_system_email');
        // $attachments = [
        //     FCPATH.'/uploads/exam_report_pdf/exam_report_2C9EE6993B4B7652F9230E321600435B.pdf'
        // ];
        $send_system_email_url = 'php send_system_email.php "'.urlencode($to).'" "'.urlencode($subject).'" "'.urlencode($message).'"';
        if($attachments) {
            $send_system_email_url = $send_system_email_url.' "'.urlencode(json_encode($attachments)).'"';
        }

        if(in_array(DOMAIN, ['http://uat.theaccountingbaba.com','https://theaccountingbaba.com', 'https://www.theaccountingbaba.com'])) {
            $send_system_email_url = $send_system_email_url . ' > /dev/null 2>&1 &';
            // $send_system_email_url = $send_system_email_url . ' > /var/www/html/application/logs/'.microtime(true).'.email'.' 2>&1 &';
            // echo $send_system_email_url;exit;
            exec($send_system_email_url);
            return true;
            exit;
        } else {
            $send_system_email_url = $send_system_email_url . ' > C:\\xampp\\htdocs\\accountingbaba\\application\\logs\\'.microtime(true).'.email';
        }

        // old code
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.zoho.com';
        $config['smtp_port'] = '465';
        $config['smtp_crypto'] = 'ssl';
        // $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'noreply@theaccountingbaba.com';
        $config['smtp_pass'] = 'Norep@208';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html

        $this->email->initialize($config);
        $this->email->from($this->config->item('email_from'), 'Accounting Baba');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        if(is_array($attachments)) {
            foreach($attachments as $attachment) {
                if(file_exists($attachment)) {
                    $this->email->attach($attachment);
                }
            }
        }

        if(!$this->email->send()) {
            $this->store_logs($this->email->print_debugger());
            return false;
        }
        return true;
    }

    /**
     *
     * @param string $to email to
     * @param string $subject email subject
     * @param string $message email message
     * @param array $attachments email attachments
     * @return boolean
     */
    public function sendMail($to, $subject, $message, $attachments = null) {
        $mail = new PHPMailer(true);
        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'smtp.zoho.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'noreply@theaccountingbaba.com';
            $mail->Password = 'Norep@208';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            $mail->setFrom($this->config->item('email_from'), 'Accounting Baba');
            $mail->addAddress($to);

            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    = $message;

            //Attachments
            if(is_array($attachments)) {
                foreach($attachments as $attachment) {
                    if(file_exists($attachment)) {
                        $mail->addAttachment($attachment);
                    }
                }
            }

            if(!$mail->send()) {
                $this->store_logs($mail->ErrorInfo);
                return false;
            }
            return true;
        } catch (Exception $e) {
            $this->store_logs($e->getMessage());
        }
        return false;
    }

    /**
     * Store logs
     *
     * @param mixed $data
     * @return void
     */
    public function store_logs($data = null) {
        $result = "";
        $result = $result."[".date('Y-m-d H:i:s')."]"."\n";
        // $result = $result."controller: ".$this->router->fetch_class()."\n";
        // $result = $result."method: ".$this->router->fetch_method()."\n";
        $result = $result . "uri_string: ".$this->uri->uri_string."\n";
        $result = $result . "get: ".json_encode($_GET)."\n";
        $result = $result . "post: ".json_encode($_POST)."\n";
        $result = $result . "request: ".json_encode($_REQUEST)."\n";
        $result = $result . "files: ".json_encode($_FILES)."\n";
        $result = $result . "http_user_agent: ". ( isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "" ) . "\n";
        $result = $result . "remote_addr: ". ( isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "" ) . "\n";
        $result = $result . "http_referer: ". ( isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "" ) . "\n";

        if($data != null) {
            $result = $result.json_encode($data)."\n";
        }

        $result = $result."\n";
        $log_file_name = "log_".date('Y-m-d').".txt";
        $log_file_path = APPPATH.'logs/'.$log_file_name;
        file_put_contents($log_file_path, $result, FILE_APPEND);
    }
    /**
     * generate accessToken
     */
    public function generateAccessToken($userID = '')
    {
        //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySUQiOiIifQ.n15J3zZM7mQ7Wz8LkWoSY9vhZFMTRkLM-bMMd91QeIs
        $this->load->library('JWT');
        $token = array();
        $token['userID'] = $userID;
        return JWT::encode($token, JWT_KEY);
    }
            /**
     * check accessToken
     */
    public function validateAccessToken($apiHeader = False,$userID = '', $initialAPI = False)
    {
        try {
            $this->load->library('JWT');
            if ($apiHeader == False) {
               throw new Exception(ACCESS_DENIED);
            }

            if (!isset($apiHeader['accessToken']) && $apiHeader['accessToken'] == '') {
                throw new Exception(ACCESS_DENIED);
            }
            $accessToken = JWT::decode($apiHeader['accessToken'], JWT_KEY);
         
            if (!isset($accessToken->userID)) {
                throw new Exception(ACCESS_DENIED);
            }
        
            if ($initialAPI == False) {
                if ($accessToken->userID != $userID) {
                    throw new Exception(ACCESS_DENIED);
                }
            } else {
                if ($accessToken->userID != '' && $accessToken->userID != $userID) {
                    throw new Exception(ACCESS_DENIED);
                }
            }
        }
        catch(Exception $e){
            $data = array(
                "code" => StatusCodes::ACCESS_DENIED,
                "message" => $e->getMessage()
            );
            $data = json_encode($data);
            $this->output->set_content_type('application/json')->set_output($data)->_display();exit();
        }
    }
    /**
     * check accessToken
     */
    public function validateRequest()
    {
        if(IS_PRODUCTION) {
             $initialAPI = ['api/sendotp','api/resend_otp'];
             $apiHeader = getallheaders();
             $userID = isset($_REQUEST['userID']) ? $_REQUEST['userID'] : "";
            if (in_array($this->router->fetch_class() . '/' . $this->router->fetch_method(), $initialAPI)) {
               $this->validateAccessToken($apiHeader, $userID,true);
            }else{
                $this->validateAccessToken($apiHeader, $userID);
            }

        }
    }

       /**
     * Role Access
     */
    function getRoleAccessLists() {
        $CI =& get_instance();
    
        $role_details=$CI->CommonModel->get_row(TBL_ADMIN_ROLE,['GROUP_CONCAT(pageUrl) as pageUrls'],['adminType'=>$CI->session->userdata('admin')['adminType']]);
       // print_r($role_details);die;
        $result=array();
        if(!empty($role_details['pageUrls'])){
           $result = explode(",",$role_details['pageUrls']);
        }
        return $result;
    }

    /**
     * set Data received
     */
    public function setRequestData()
    {
        $postData = array();
        if(!empty($_REQUEST)) {
            foreach ($_REQUEST as $key => $value) {
                $postData[$key] = addslashes(trim($value));
            }
        }
        return $postData;
    }

}


/**
 * Extend MY_Controller class
 *
 * @author suresh saini
 */
class Admin_Controller extends MY_Controller {

    protected $loginAdminID;
    protected $data = array(),$page_js = array(),$page_css = array();

    function __construct() {

        parent::__construct();

        $this->data['title'] = SITE_NAME;
        $this->load_css();
        $this->load_js();

        if(!$this->session->userdata('admin')) {
            redirect(site_url().'admin/auth');
        }

        $this->loginAdminID = $this->session->userdata('admin')['adminID'];

        /**Check for Role Access of this url start */
        $url_string = $this->uri->segment(2);
        if($this->uri->segment(3))
        $url_string .= "/".$this->uri->segment(3);

        $this->roleAccessList=$this->getRoleAccessLists();
        if(empty($this->roleAccessList)){
            redirect('admin/logout');
        }
        if($this->roleAccessList[0] != 'ALL'){
            if(!in_array($url_string,$this->roleAccessList)){
                echo "No Access To : ".base_url()."admin/".$url_string;
                redirect("admin/".$this->roleAccessList[0]);
            }
        }
        /**END */
    }

    private function load_css()
    {
        $_css='';
        foreach ($this->page_css as $css)
        {
            $_css .= '<link href="'.ADMIN_ASSETS_URL.$css.'" rel="stylesheet">';
        }
        $this->data['add_css'] = $_css;
    }
    private function load_js()
    {
        $_js='';
        foreach ($this->page_js as $js)
        {
            $_js.='<script type="text/javascript" src="'.ADMIN_ASSETS_URL.$js.'?v="'.FILE_VERSION.'"></script>';
        }
        $this->data['add_js'] = $_js;
    }



}