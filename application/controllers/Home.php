<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends My_Controller {

    public function __construct() {

        parent::__construct();
    }

    public function aboutus(){
        $this->data['title'] = SITE_NAME.' | About us';
        $this->template->load('site_layout', 'home/aboutus', $this->data);
    }

    public function termsandconditions(){
        $this->data['title'] = SITE_NAME.' | Terms and conditions';
        $this->template->load('site_layout', 'home/termsandconditions', $this->data);
    }
    
    public function privacy_policy(){
        $this->data['title'] = SITE_NAME.' | Privacy Policy';
        $this->template->load('site_layout', 'home/privacypolicy', $this->data);
    }

}
