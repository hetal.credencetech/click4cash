<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Razorpay\Api\Api as Razorpay;
class Api extends My_Controller {

    public function __construct() {

        parent::__construct();
        $this->validateRequest();
        $this->requestData = $this->setRequestData();
        $this->load->model('UserModel','user');
        header('Content-Type: application/json');
        //Check Kyc Upload Authentication
        $ignorKycMethod = ['sendotp','verify','add_kyc_detail','resend_otp','bank_account_list','add_bank_account','remove_bank_account','transaction_list','notification_list'];
        $ignorVarifyMethod = ['sendotp','verify','resend_otp'];
        if (!in_array($this->router->fetch_method(), $ignorVarifyMethod)) {
            $this->init('verify');
        }

        if (!in_array($this->router->fetch_method(), $ignorKycMethod)) {
           // $this->init('kyc');
        }
    }

    private function init($authBy)
    {
        $userID = getKeyValue("userID","");
        if (!isset($userID) || empty($userID))
        throw new Exception('please pass UserID');
        
        if($authBy == 'verify')
        {
            $userDetails = $this->user->getAuthenticateUser($userID);
            if(empty($userDetails)){
                $data = array(
                    "code" => StatusCodes::FAILURE,
                    "message" => 'User is not varify.'
                );
                echo json_encode($data);die;
                
            }
       } else if($authBy == 'kyc'){
            $userDetails = $this->user->getKycAuthenticateUser($userID);
            if(empty($userDetails)){
                $data = array(
                    "code" => StatusCodes::KYC_DENIED,
                    "message" => 'Your KYC Details is not completed Or not approved from admin.'
                );
                echo json_encode($data);die;
            }
       }
    }


    public function sendotp(){
        $post_body = file_get_contents('php://input');
        file_put_contents("raw_body_logs.txt",$post_body);
        file_put_contents("get_logs.txt",json_encode($_GET));
        file_put_contents("post_logs.txt",json_encode($_POST));
        try {
            $deviceType = (int)getKeyValue("deviceType","");
            $deviceToken = getKeyValue("deviceToken","");
            $deviceID = getKeyValue("deviceID","");
            $phoneNumber = getKeyValue("phoneNumber","");

            if (!isset($deviceType) || in_array($deviceType, [DeviceType::ANDROID,DeviceType::IOS]) == false)
            throw new Exception('Please Pass Device Type');
            
            if (!isset($deviceID) || empty($deviceID))
            throw new Exception('Please Pass Device ID');
            if (!isset($phoneNumber) || empty($phoneNumber))
            throw new Exception('Please Pass Phone number');

                $userExist = $this->CommonModel->get_row(TBL_USER,'userID,isVerified,otp',array('phoneNumber' => $phoneNumber));
                $otp = generateRandomCode(4);
                if(empty($userExist))
                {
                    $insertData = [];
                    $insertData['phoneNumber'] = $phoneNumber;
                    $insertData['deviceType'] = $deviceType;
                    $insertData['deviceToken'] = $deviceToken;
                    $insertData['deviceID'] = $deviceID;
                    $insertData['otp'] = $otp;
                    $userID = $this->CommonModel->insert(TBL_USER,$insertData);
                    if(!$userID)
                        throw new Exception();
                  
                }
                else{
                    $userID = $userExist['userID'];
                    $otp = ($userExist['otp'] == '' || $userExist['otp'] == null)?$otp:$userExist['otp']; 

                    //Update device details
                    $updateData['deviceType'] = $deviceType;
                    $updateData['deviceToken'] = $deviceToken;
                    $updateData['deviceID'] = $deviceID;
                    $updateData['otp'] = $otp;
                    $this->CommonModel->update(TBL_USER, $updateData,['userID'=>$userID]);
                }
               
                /*send OTP*/
                $messageOTP = $otp . " is your otp to verify on click4cash.";
                sendSMS($phoneNumber,$messageOTP);
                /*end*/
                $data = array();
                $data['userID'] = $userID;
                $data['accessToken'] = $this->generateAccessToken($userID);

                $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "OTP send Successfully Please Verify.","userData" => $data));

        }catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
    }
    //for Register new User
    public function resend_otp()
    {
        try {
        $phoneNumber = getKeyValue("phoneNumber","");

        if (!isset($phoneNumber) || empty($phoneNumber))
        throw new Exception('please pass phoneNumber');
         
       
         $userData = array();
         $userData['otp'] =  generateRandomCode(4);
         $checkMobileExist = $this->CommonModel->get_row(TBL_USER,'userID,isVerified,otp',array('phoneNumber' => $phoneNumber));
         if(empty($checkMobileExist ))
         throw new Exception('Phone Number is not register');

         $this->CommonModel->update(TBL_USER, $userData,['userID'=>$checkMobileExist['userID']]);
         
          /*send OTP*/
          $messageOTP = $userData['otp'] . " is your otp to verify on click4cash.";
          sendSMS($phoneNumber,$messageOTP);
          /*end*/
          $data = array();
          $data['userID'] = $checkMobileExist['userID'];

          $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "OTP send Successfully Please Verify.","userData" => $data));

        }catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
     }
     

     public function verify()
     {
         try {
            $userID = getKeyValue("userID","");
            $otp = getKeyValue("otp","");
    
            if (!isset($userID) || empty($userID))
            throw new Exception('please pass UserID');
    
            if (!isset($otp) || empty($otp))
            throw new Exception('please pass otp');
             
                $userExist = $this->CommonModel->get_row(TBL_USER,'userID,otp',array('userID' => $userID));
                if(empty($userExist))
                throw new Exception('User Not Found');
                    
                if($userExist['otp'] != $otp && $otp != '1234' )
                throw new Exception('Otp is not match');
    
                   
                $kycUploaded = KycUploaded::NO;
                $kycStatus = KycStatus::NOTAPPROVE;
                //Update device details
                $updateData['isVerified'] = IsVerified::YES;
                $this->CommonModel->update(TBL_USER, $updateData,['userID'=>$userID]);
    
                $userKyc = $this->CommonModel->get_row(TBL_USER_KYC,'kycID,aadharCardFrontImage,aadharCardBackImage,userImage,status',array('userID' => $userID));
                if(!empty($userKyc) && $userKyc['aadharCardFrontImage'] != '' && $userKyc['aadharCardBackImage'] != '' && $userKyc['userImage'] != '')
                {
                $kycUploaded = KycUploaded::YES;
                $kycStatus = $userKyc['status'];
                }
                   
                    $data = array();
                    $data['userID'] = $userID;
                    $data['isVerified'] = IsVerified::YES;
                    $data['kycStatus'] = $kycStatus;
                    $data['kycUploaded'] = $kycUploaded;
    
                    $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "verify Successfully","userData" => $data));
         }catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
     }
     

    public function add_kyc_detail(){

        try {
        
            $userID = getKeyValue("userID","");
            $aadharCardFrontImage = isset($_FILES['aadharCardFrontImage'])?$_FILES['aadharCardFrontImage']:"";
            $aadharCardBackImage = isset($_FILES['aadharCardBackImage'])?$_FILES['aadharCardBackImage']:"";
            $userImage = isset($_FILES['userImage'])?$_FILES['userImage']:"";

            if (!isset($aadharCardFrontImage) || empty($aadharCardFrontImage))
            throw new Exception('Please Pass aadharCard FrontImage');
            if (!isset($aadharCardBackImage) || empty($aadharCardBackImage))
            throw new Exception('Please Pass aadharCard BackImage');
            if (!isset($userImage) || empty($userImage))
            throw new Exception('Please Pass user Image');
            if (!isset($userID) || empty($userID))
            throw new Exception('Please Pass userID');

            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['encrypt_name'] = FALSE;
            $config['remove_spaces'] = TRUE;
            $config['overwrite'] = TRUE;
            $config['max_size'] = '1024000';
            $config['file_name']  = $userID;
            $updateData=[];
            if (!empty($_FILES['aadharCardFrontImage'])) {
                $config['upload_path'] = UPLOAD_PATH.KYC_FRONT_UPLOAD_PATH;
                $this->upload->initialize($config);
                    if (!$this->upload->do_upload('aadharCardFrontImage')) {
                        throw new Exception($this->upload->display_errors());
                    } else {
                        $fileData = $this->upload->data();
                        $updateData['aadharCardFrontImage']=$fileData["file_name"];
                }
            }
            if (!empty($_FILES['aadharCardBackImage'])) {
                $config['upload_path'] = UPLOAD_PATH.KYC_BACK_UPLOAD_PATH;
                $this->upload->initialize($config);
                    if (!$this->upload->do_upload('aadharCardBackImage')) {
                        throw new Exception($this->upload->display_errors());
                    } else {
                        $fileData = $this->upload->data();
                        $updateData['aadharCardBackImage']=$fileData["file_name"];
                }
            }
            if (!empty($_FILES['userImage'])) {
                $config['upload_path'] = UPLOAD_PATH.USER_IMAGE_UPLOAD_PATH;
                $this->upload->initialize($config);
                    if (!$this->upload->do_upload('userImage')) {
                        throw new Exception($this->upload->display_errors());
                    } else {
                        $fileData = $this->upload->data();
                        $updateData['userImage']=$fileData["file_name"];
                }
            }

          
                $userKycExist = $this->CommonModel->get_row(TBL_USER_KYC,'kycID,status',array('userID' => $userID));
           
                if(empty($userKycExist))
                {
                    $updateData['userID'] = $userID;
                    $kycID = $this->CommonModel->insert(TBL_USER_KYC,$updateData);

                    $status=KycStatus::NOTAPPROVE;
                    if(!$kycID)
                        throw new Exception();
                  
                }
                else{
                    $kycID = $userKycExist['kycID'];
                    if($userKycExist['status'] != KycStatus::APPROVED)
                    $this->CommonModel->update(TBL_USER_KYC, $updateData,['kycID'=>$kycID]);

                    $status=$userKycExist['status'];
                }
               
                $data = array();
                $data['kycID'] = $kycID;
                $data['status'] = $status;

                $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "Kyc Uploaded Successfully","userKycData" => $data));

        }catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
    }


    public function get_kyc_detail(){

        try {
        
            $userID = getKeyValue("userID","");
            $userKycData = $this->CommonModel->get_row(TBL_USER_KYC,'kycID,status,IF(aadharCardFrontImage != "",concat("'.UPLOAD_URL.KYC_FRONT_UPLOAD_PATH.'",aadharCardFrontImage),"") as  aadharCardFrontImage,IF(aadharCardBackImage != "",concat("'.UPLOAD_URL.KYC_BACK_UPLOAD_PATH.'",aadharCardBackImage),"") as  aadharCardBackImage,IF(userImage != "",concat("'.UPLOAD_URL.USER_IMAGE_UPLOAD_PATH.'",userImage),"") as  userImage',array('userID' => $userID));
            $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "Kyc Details Successfully","userKycData" => $userKycData));

        }catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
    }

    public function bank_account_list(){

        try {
            $userID = getKeyValue("userID","");
            if (!isset($userID) || empty($userID))
            throw new Exception('please pass UserID');

            $where = [];
            $where['userID'] = $userID;
            $bankAccountList = $this->CommonModel->get_all(TBL_BANK_ACCOUNT,'bankAccountID,ifscCode,accountNumber,accountName,bankName,createdOn',$where);
            $purposeList = $this->CommonModel->get_all(TBL_PURPOSE,'purposeID,purpose,createdOn');
            
            $transactionLimitMessage='';
            $todayTransactionList = $this->user->getTransactionList(['tu.userID'=>$userID,'DATE(t.createdOn)'=>date('Y-m-d')]);
           
            if(!empty($todayTransactionList) && count($todayTransactionList) >= 10000)
            $transactionLimitMessage = 'Your daily limit of transaction is done so please try next transaction on next day.';
           
            $kycUploaded = KycUploaded::NO;
            $kycStatus = "".KycStatus::NOTAPPROVE;
            
            $userKyc = $this->CommonModel->get_row(TBL_USER_KYC,'kycID,aadharCardFrontImage,aadharCardBackImage,userImage,status',array('userID' => $userID));
            if(!empty($userKyc) && $userKyc['aadharCardFrontImage'] != '' && $userKyc['aadharCardBackImage'] != '' && $userKyc['userImage'] != '')
            {
            $kycUploaded = KycUploaded::YES;
            $kycStatus = "".$userKyc['status'];
            }
            
            $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "Bank Account Get Successfully","bankAccountList" => $bankAccountList,"purposeList"=>$purposeList,"kycStatus"=>$kycStatus,"kycUploaded"=>$kycUploaded,"transactionLimitMessage"=>$transactionLimitMessage));
        } catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
    }

    public function add_bank_account(){

        try {

            $userID = getKeyValue("userID","");
            $ifscCode = getKeyValue("ifscCode","");
            $accountNumber = getKeyValue("accountNumber","");
            $accountName = getKeyValue("accountName","");
            $bankName = getKeyValue("bankName","");

            if (!isset($userID) || empty($userID))
            throw new Exception('please pass UserID');
            if (!isset($ifscCode) || empty($ifscCode))
            throw new Exception('please pass ifsc Code');
            if (!isset($accountNumber) || empty($accountNumber))
            throw new Exception('please pass account Number');
            if (!isset($accountName) || empty($accountName))
            throw new Exception('please pass account Name');
            if (!isset($bankName) || empty($bankName))
            throw new Exception('please pass bank Name');

            $alreadyExistBankAccount = $this->CommonModel->get_row(TBL_BANK_ACCOUNT,'bankAccountID',['userID'=>$userID,'accountNumber'=>$accountNumber]);
            if(!empty($alreadyExistBankAccount)){
                throw new Exception('You already added this account number details.');
            }


            $arrInsert= [];
            $arrInsert['userID']=$userID;
            $arrInsert['ifscCode']=$ifscCode;
            $arrInsert['accountNumber']=$accountNumber;
            $arrInsert['accountName']=$accountName;
            $arrInsert['bankName']=$bankName;
            
            $bankAccountID = $this->CommonModel->insert(TBL_BANK_ACCOUNT, $arrInsert);
            $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "Bank Account Added Successfully"));
        } catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
    }

    public function remove_bank_account(){
    
            try {
    
                $userID = getKeyValue("userID","");
                $bankAccountID = getKeyValue("bankAccountID","");
    
                if (!isset($userID) || empty($userID))
                throw new Exception('please pass UserID');
                if (!isset($bankAccountID) || empty($bankAccountID))
                throw new Exception('please pass bank AccountID');
    
                $alreadyExistBankAccount = $this->CommonModel->get_row(TBL_BANK_ACCOUNT,'bankAccountID',['bankAccountID'=>$bankAccountID]);
                if(empty($alreadyExistBankAccount)){
                    throw new Exception('Bank account not exist.');
                }
                
                $bankAccountID = $this->CommonModel->set_delete(TBL_BANK_ACCOUNT, ['bankAccountID'=>$bankAccountID]);
                $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "Bank Account Deleted Successfully"));
            } catch (Exception $e){
                $this->set_error_response($e->getMessage());
            }
        }

    public function transaction_list(){

        try {
            $userID = getKeyValue("userID","");
            if (!isset($userID) || empty($userID))
            throw new Exception('please pass UserID');

            $where = [];
            $where['tu.userID'] = $userID;
            $bankAccountList = $this->user->getTransactionList($where);
            $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "Bank Account Get Successfully","bankAccountList" => $bankAccountList));
        } catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
    }
    public function insert_payment() {
        try {
            $payment_id = getKeyValue("razorpay_payment_id","");
            $amount = getKeyValue("amount","");
            $purpose = getKeyValue("purpose","");
            $gst = getKeyValue("gst",0);
            $serviceTax = getKeyValue("serviceTax",0);
            $taxType = getKeyValue("taxType","");
            $grossAmount = getKeyValue("grossAmount","");
            $description = getKeyValue("description","");
            $bankAccountID = getKeyValue("bankAccountID","");
            $userID = getKeyValue("userID","");

            if (!isset($userID) || empty($userID))
            throw new Exception('please pass UserID');

            if (!isset($payment_id) || empty($payment_id))
            throw new Exception('please pass razorpay payment id');
         
            if (!isset($grossAmount) || empty($grossAmount))
            throw new Exception('please pass gross razorpay amount');

            if (!isset($purpose) || empty($purpose))
            throw new Exception('please pass purpose');

            if (!isset($amount) || empty($amount))
            throw new Exception('please pass amount');


            if (!isset($bankAccountID) || empty($bankAccountID))
            throw new Exception('please pass bank account id');

            $api = new Razorpay(RAZORPAY_KEY_ID, RAZORPAY_KEY_SECRET);
            $datetime = date('Y-m-d H:i:s');
            
            // Captures a payment
            $payment = $api->payment->fetch($payment_id)->capture(['amount'=> $grossAmount * 100]); 
           
            // transaction data
            $transaction_data = [
                'bankAccountID'=>$bankAccountID,
                'rpTxnID' => $payment->id,
                'description' =>  $description ,
                'amount' => $amount,
                'grossAmount' => $payment->amount,
                'gst' => $gst,
                'serviceTax' => $serviceTax,
                'taxType' => $taxType,
                'purpose' => $purpose,
                'createdBy' => $userID
            ];
            $lastTransactionID = $this->CommonModel->insert(TBL_TRANSACTION, $transaction_data);
            $this->sendNotification([
                "title"=>"withdrawn",
                "message"=>"₹ ".$payment->amount." is withdrawn on ".getDefaultDate(),
                "createdBy"=>$userID
            ]);
            $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "Payment Successfully"));
        } catch (Exception $e) {
            $this->set_error_response($e->getMessage());
        }
    }


    public function sendNotification($notificationData = array())
    { 
        $notificationIns = array();
        $userNotificationData = array();
    
        $notificationIns['title'] = isset($notificationData['title'])?$notificationData['title']:'Click4Cash';
        $notificationIns['message'] = isset($notificationData['message'])?$notificationData['message']:'Test Click4Cash';
        $notificationIns['createdBy'] = isset($notificationData['createdBy'])?$notificationData['createdBy']:1;
      
        // INSERT AFTER NOTIFICATION MASTER SEND
           $notificationID = $this->CommonModel->insert(TBL_NOTIFICATION,$notificationIns);
        
        $userID = $this->CommonModel->get_all(TBL_USER,['userID'],["userID"=>$notificationIns['createdBy']]);
        foreach($userID as $user){
                $user = $user['userID'];
                $userData = $this->CommonModel->get_row(TBL_USER,["deviceToken","deviceType","deviceID"],["userID"=>$user]);
                $this->CommonModel->insert(TBL_NOTIFICATION_DEVICE,["notificationID"=>$notificationID,"userID"=>$user]);   
                $Message=array(
                    'Title'=>$notificationIns['title'],
                    'body'=>$notificationIns['message'],
                   // 'notificationType'=>$notificationIns['type'],
                    'notificationID'=>$notificationID,
                    'targetID'=>$notificationIns['createdBy'],
                );
                if($userData['deviceType'] == 1){
                    $this->CommonModel->sendAndroidNotification($Message,$userData['deviceToken'],$userData['deviceToken'],$userData['deviceID']);
                }else if($userData['deviceType'] == 2){
                    $this->CommonModel->sendAppleNotification($Message,$userData['deviceToken'],$userData['deviceToken'],$userData['deviceID']);
                }
        }   
    }


    public function notification_list(){
        try {
            $userID = getKeyValue("userID","");

            if (!isset($userID) || empty($userID))
            throw new Exception('please pass UserID');

            $where = array('tnd.userID' => $userID);
            $notifications = $this->user->getUserNotification($where);
            $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "Notification Get Successfully","notificationList" => $notifications));
        
        }catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
    }


    
    public function calculate_tax(){
        try {
            $userID = getKeyValue("userID","");
            $amount = getKeyValue("amount","");

            if (!isset($userID) || empty($userID))
            throw new Exception('please pass UserID');

            if (!isset($amount) || empty($amount))
            throw new Exception('please pass amount');



            $taxDetails = $this->CommonModel->get_all(TBL_CONFIG,'configKey,configValue',' `configKey` IN ("gst","serviceTax","taxType")');
            $taxArr = [];
            foreach($taxDetails as $key =>$value){
                    $taxArr[$value['configKey']]=$value['configValue'];
            }

            $gst=0;
            $serviceTax=0;
            if($taxArr['taxType'] == 'percentage'){
                $serviceTax = ($amount * $taxArr['serviceTax'])/100;
                $gst = ($serviceTax * $taxArr['gst'])/100;
            }elseif($taxArr['taxType'] == 'flat'){
                $serviceTax = $taxArr['serviceTax'];
                $gst = $taxArr['gst'];
            }

            $grossAmount = $amount + $gst + $serviceTax;

            $data['amount'] =(float)$amount; 
            $data['serviceTax'] =round((float)$serviceTax,2);
            $data['gst'] =round((float)$gst,2);  
            $data['grossAmount'] =round($grossAmount,2);
            $data['taxType'] =$taxArr['taxType'];
            $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "Tax Get Successfully","taxData" => $data));
        
        }catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
    }
    
    
    public function add_user_enquiry(){

        try {
        
            $userID = getKeyValue("userID","");
            $subject = getKeyValue("subject","");
            $description = getKeyValue("description","");
            $attachment = isset($_FILES['attachment'])?$_FILES['attachment']:"";

            if (!isset($subject) || empty($subject))
            throw new Exception('Please Pass Subject');
            if (!isset($description) || empty($description))
            throw new Exception('Please Pass Description');            
            if (!isset($userID) || empty($userID))
            throw new Exception('Please Pass userID');

            $config['allowed_types'] = '*';
            $config['encrypt_name'] = FALSE;
            $config['remove_spaces'] = TRUE;
            $config['overwrite'] = TRUE;
            $config['max_size'] = '1024000';
            $insertData=[];
            if (!empty($_FILES['attachment']['name'])) {
                $config['file_name'] = microtime() . "." . pathinfo($_FILES['attachment']['name'], PATHINFO_EXTENSION);
                $config['upload_path'] = UPLOAD_PATH.ATTACHMENT_UPLOAD_PATH;
                $this->upload->initialize($config);
                    if (!$this->upload->do_upload('attachment')) {
                        throw new Exception($this->upload->display_errors());
                    } else {
                        $fileData = $this->upload->data();
                        $insertData['attachment']=$fileData["file_name"];
                }
            }

            $insertData['userID'] = $userID;
            $insertData['subject'] = $subject;
            $insertData['description'] = $description;
            $userenquiryID = $this->CommonModel->insert(TBL_USER_ENQUIRY,$insertData);

            $data = array();
            $data['userenquiryID'] = $userenquiryID;

            $this->set_response(array("code" => StatusCodes::SUCCESS, "message" => "Thank you for your enquiry Your message has been sent successfully.","userEnquiryData" => $data));

        }catch (Exception $e){
            $this->set_error_response($e->getMessage());
        }
    }

}
