<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_Controller {
    
    public function __construct() {
        $this->page_js[] = 'global_assets/js/plugins/forms/inputs/jquery.mask.js';
        $this->page_js[] = 'assets/js/ckeditor.js';
        $this->page_js[] = 'global_assets/js/plugins/media/fancybox.min.js';
        $this->page_js[] = 'assets/script/user.js';
        parent::__construct();
        $this->load->model('UserModel','user');
    }
    
    /**
    * user page
    *
    * @return void
    */
    public function index() {
        $this->data['title'] = SITE_NAME.' | User';
        $this->data['page_heading'] = 'User List';
        $this->template->load('admin_layout', 'admin/user/index', $this->data);
    }
  
    public function ajax_user_list() {
        $columns = array('userID','phoneNumber','isVerified','otp');
        $orderBy = $_POST['columns'][$_POST['order'][0]['column']]['data'];
        $sortBy = $_POST['order'][0]['dir'];
        $search = trim($_POST['search']['value']);
        $searchData = "1 = 1";
        if ($search != "") {
            $search = addslashes($search);
           // $searchData = "( name LIKE '%" . $search . "%')";
        }
        $data = [
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->CommonModel->get_num_rows(TBL_USER, "1 = 1"),
            "recordsFiltered" => $this->CommonModel->get_num_rows(TBL_USER,$searchData),
            "data" => $this->CommonModel->get_all(TBL_USER,$columns,$searchData,$orderBy,$sortBy,$_POST['start'],$_POST['length'])
       
        ];
        $this->output->set_content_type('application/json')
        ->set_output(json_encode($data));
    }


    public function delete_user($ID) {
        try {
            if(empty($ID)) {
                throw new Exception("Parameter is missing!");
            }
            $userRow = $this->CommonModel->get_row(TBL_USER,'userID',['userID' => $ID]);

            if(empty($userRow))
                throw new Exception('No User Found');

            $deletedRecords = $this->CommonModel->set_delete(TBL_USER, ['userID' => $ID]);
            
            if($deletedRecords <= 0 ) {
                throw new Exception("Error occurred at server side while deleting record!");
            }
            
            $response_array = [
                'code' => 100,
                'status' => 'success',
                'message' => $deletedRecords.' Record(s) have been deleted.'
            ];
            
        } catch(Exception $e) {
            $this->set_error_response($e->getMessage());
        }
        $this->output->set_content_type('application/json')
        ->set_output(json_encode($response_array));
    }




    public function transaction() {
        $this->data['title'] = SITE_NAME.' | Transaction';
        $this->data['page_heading'] = 'Tansaction List';
        $this->template->load('admin_layout', 'admin/user/transaction', $this->data);
    }
  
    public function ajax_transaction_list() {
        $orderBy = $_POST['columns'][$_POST['order'][0]['column']]['data'];
        $sortBy = $_POST['order'][0]['dir'];
        $search = trim($_POST['search']['value']);
        $searchData = "1 = 1";
        if ($search != "") {
            $search = addslashes($search);
           // $searchData = "( name LIKE '%" . $search . "%')";
        }
        $data = [
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->user->getTransactionList("1 = 1",$orderBy,$sortBy,0, null, true),
            "recordsFiltered" => $this->user->getTransactionList($searchData,$orderBy,$sortBy,0, null, true),
            "data" => $this->user->getTransactionList($searchData,$orderBy,$sortBy,$_POST['start'],$_POST['length'])
       
        ];
        $this->output->set_content_type('application/json')
        ->set_output(json_encode($data));
    }


    public function delete_transaction($ID) {
        try {
            if(empty($ID)) {
                throw new Exception("Parameter is missing!");
            }
            $row = $this->CommonModel->get_row(TBL_TRANSACTION,'transactionID',['transactionID' => $ID]);

            if(empty($row))
                throw new Exception('No Transaction Found');

            $deletedRecords = $this->CommonModel->set_delete(TBL_TRANSACTION, ['transactionID' => $ID]);
            
            if($deletedRecords <= 0 ) {
                throw new Exception("Error occurred at server side while deleting record!");
            }
            
            $response_array = [
                'code' => 100,
                'status' => 'success',
                'message' => $deletedRecords.' Record(s) have been deleted.'
            ];
            
        } catch(Exception $e) {
            $this->set_error_response($e->getMessage());
        }
        $this->output->set_content_type('application/json')
        ->set_output(json_encode($response_array));
    }

    public function pending_transaction_list() {
        $this->data['title'] = SITE_NAME.' | Pending Transaction';
        $this->data['page_heading'] = 'Pending Tansaction List';
        $this->template->load('admin_layout', 'admin/user/pending_transaction', $this->data);
    }
  
    public function ajax_pending_transaction_list() {
        $orderBy = $_POST['columns'][$_POST['order'][0]['column']]['data'];
        $sortBy = $_POST['order'][0]['dir'];
        $search = trim($_POST['search']['value']);
        $searchData = "t.status = 0";
        if ($search != "") {
            $search = addslashes($search);
           // $searchData = "( name LIKE '%" . $search . "%')";
        }
        $data = [
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->user->getTransactionList("1 = 1",$orderBy,$sortBy,0, null, true),
            "recordsFiltered" => $this->user->getTransactionList($searchData,$orderBy,$sortBy,0, null, true),
            "data" => $this->user->getTransactionList($searchData,$orderBy,$sortBy,$_POST['start'],$_POST['length'])
       
        ];
        $this->output->set_content_type('application/json')
        ->set_output(json_encode($data));
    }


    public function update_status_transaction()
    {
        try {
            // pr($_POST);
            if(!$this->input->post()) {
                throw new Exception("No input data!");
            }
           
            $updateData['remark'] = $this->input->post('remark'); 
            $updateData['status'] = $this->input->post('status');
            $this->CommonModel->update(TBL_TRANSACTION, $updateData ,['transactionID'=>$this->input->post('transactionID')]);
            $response_array = [
                'code' => 100,
                'status' => 'success',
                'message' => "Record has been updated."
            ];
        } catch (Exception $e) {
            $response_array = [
                'code' => 101,
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
        $this->set_response($response_array);
    }


    public function kyc_request() {
        $this->data['title'] = SITE_NAME.' | Kyc Request';
        $this->data['page_heading'] = 'Kyc Request List';
        $this->template->load('admin_layout', 'admin/user/kyc', $this->data);
    }
  
    public function ajax_kyc_list() {
        $columns = array('uk.kycID','u.userID','u.phoneNumber','u.isVerified','If(uk.status =  0,"Unapproved","Approved") as status','IF(uk.aadharCardFrontImage != "",concat("'.KYC_FRONT_UPLOAD_PATH.'",uk.aadharCardFrontImage),"") as  aadharCardFrontImage','IF(uk.aadharCardBackImage != "",concat("'.KYC_BACK_UPLOAD_PATH.'",uk.aadharCardBackImage),"") as  aadharCardBackImage','IF(uk.userImage != "",concat("'.USER_IMAGE_UPLOAD_PATH.'",uk.userImage),"") as  userImage,uk.remarks,uk.createdOn');
        $orderBy = isset($_POST['columns'])?$_POST['columns'][$_POST['order'][0]['column']]['data']:$_POST['orderBy'];
        $sortBy = isset($_POST['order'])?$_POST['order'][0]['dir']:$_POST['sortBy'];
        $search = trim(isset($_POST['search']['value'])?$_POST['search']['value']:$_POST['search']);
        $searchData = "1 = 1";
        if ($search != "") {
            $searchData = addslashes($search);
           // $searchData = "( name LIKE '%" . $search . "%')";
        }
        $data = [
            "draw" => isset($_POST['draw'])?$_POST['draw']:'',
            "recordsTotal" => $this->user->getKycList($columns,"1 = 1",$orderBy,$sortBy,0, null, true),
            "recordsFiltered" => $this->user->getKycList($columns,$searchData,$orderBy,$sortBy,0, null, true),
            "data" => $this->user->getKycList($columns,$searchData,$orderBy,$sortBy,$_POST['start'],$_POST['length'])
       
        ];
        $this->output->set_content_type('application/json')
        ->set_output(json_encode($data));
    }


    public function delete_kyc($ID) {
        try {
            if(empty($ID)) {
                throw new Exception("Parameter is missing!");
            }
            $row = $this->CommonModel->get_row(TBL_USER_KYC,'kycID',['kycID' => $ID]);

            if(empty($row))
                throw new Exception('No Transaction Found');

            $deletedRecords = $this->CommonModel->set_delete(TBL_USER_KYC, ['kycID' => $ID]);
            
            if($deletedRecords <= 0 ) {
                throw new Exception("Error occurred at server side while deleting record!");
            }
            
            $response_array = [
                'code' => 100,
                'status' => 'success',
                'message' => $deletedRecords.' Record(s) have been deleted.'
            ];
            
        } catch(Exception $e) {
            $this->set_error_response($e->getMessage());
        }
        $this->output->set_content_type('application/json')
        ->set_output(json_encode($response_array));
    }


    public function update_status_kyc()
    {
        try {
            // pr($_POST);
            if(!$this->input->post()) {
                throw new Exception("No input data!");
            }
           
            $updateData['remarks'] = $this->input->post('remark'); 
            $updateData['status'] = $this->input->post('status');
            $this->CommonModel->update(TBL_USER_KYC, $updateData ,['kycID'=>$this->input->post('kycID')]);
            $response_array = [
                'code' => 100,
                'status' => 'success',
                'message' => "Record has been updated."
            ];
        } catch (Exception $e) {
            $response_array = [
                'code' => 101,
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
        $this->set_response($response_array);
    }


    public function kyc_unapproved_request() {
        $this->data['title'] = SITE_NAME.' | Kyc Unapproved Request';
        $this->data['page_heading'] = 'Kyc UnApproved Request List';
        $this->template->load('admin_layout', 'admin/user/kyc_unapproved_request', $this->data);
    }



    public function enquiry() {
        $this->data['title'] = SITE_NAME.' | User Enquiry';
        $this->data['page_heading'] = 'User Enquiry List';
        $this->template->load('admin_layout', 'admin/user/enquiry', $this->data);
    }
  
    public function ajax_user_enquiry_list() {
        $columns = array('ue.userenquiryID','u.userID','u.phoneNumber','IF(ue.attachment is not null,CONCAT("'.base_url()."uploads/".ATTACHMENT_UPLOAD_PATH.'",ue.attachment),"") AS attachment,ue.subject,ue.description');
        $orderBy = $_POST['columns'][$_POST['order'][0]['column']]['data'];
        $sortBy = $_POST['order'][0]['dir'];
        $search = trim($_POST['search']['value']);
        $searchData = "1 = 1";
        if ($search != "") {
            $search = addslashes($search);
           // $searchData = "( name LIKE '%" . $search . "%')";
        }
        $data = [
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->user->getEnquiryList($columns,"1 = 1",$orderBy,$sortBy,0, null, true),
            "recordsFiltered" => $this->user->getEnquiryList($columns,$searchData,$orderBy,$sortBy,0, null, true),
            "data" => $this->user->getEnquiryList($columns,$searchData,$orderBy,$sortBy,$_POST['start'],$_POST['length'])
       
        ];
       // echo $this->db->last_query();die;
        $this->output->set_content_type('application/json')
        ->set_output(json_encode($data));
    }


    public function delete_user_enquiry($ID) {
        try {
            if(empty($ID)) {
                throw new Exception("Parameter is missing!");
            }
            $userRow = $this->CommonModel->get_row(TBL_USER_ENQUIRY,'userenquiryID',['userenquiryID' => $ID]);

            if(empty($userRow))
                throw new Exception('No User Found');

            $deletedRecords = $this->CommonModel->set_delete(TBL_USER_ENQUIRY, ['userenquiryID' => $ID]);
            
            if($deletedRecords <= 0 ) {
                throw new Exception("Error occurred at server side while deleting record!");
            }
            
            $response_array = [
                'code' => 100,
                'status' => 'success',
                'message' => $deletedRecords.' Record(s) have been deleted.'
            ];
            
        } catch(Exception $e) {
            $this->set_error_response($e->getMessage());
        }
        $this->output->set_content_type('application/json')
        ->set_output(json_encode($response_array));
    }

}
