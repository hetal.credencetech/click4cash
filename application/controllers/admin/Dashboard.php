<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public $loggedInUserID;

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$data['title'] = SITE_NAME. ' | Admin Login';
		$this->template->load('admin_layout', 'admin/dashboard/index', $data);
	}

	public function logout() {
		$this->session->unset_userdata('admin');
		redirect(site_url('admin'));
	}

	public function change_password() {
		$data['title'] = SITE_NAME.' | Change Password';
		$this->template->load('admin_layout', 'admin/dashboard/change_password', $data);
	}


    /**
     * change user password
     *
     * @return void
     */
    public function do_change_password() {

        if($this->input->post()) {
            $this->form_validation->set_rules('old_password', 'Current Password', 'required|callback_check_current_password[old_password]');
            $this->form_validation->set_rules('new_password', 'New Password', 'required');
            $this->form_validation->set_rules('confirm_new_password', 'Confirm Password', 'required|matches[new_password]');

            if($this->form_validation->run()) {
                $password_data['password'] = hashString($this->input->post('new_password'));
                $this->CommonModel->update(TBL_ADMIN, $password_data, ['adminID' => $this->loginAdminID]);
				$response_array = [
					'code' => 100,
					'status' => 'success',
					'message' => "Password has been updated."
				];
            } else {
				$response_array = [
					'code' => 101,
					'status' => 'error',
					'message' => validation_errors()
				];
            }

            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response_array));
        }
    }

    /**
     * Check whether current password is valid
     *
     * @return void
     */
    public function check_password() {
        $user_data = $this->CommonModel->get_row(TBL_ADMIN, 'adminID', [
            'adminID' => $this->loginAdminID,
            'password' => hashString($this->input->post('old_password'))
            ]);
        if(!empty($user_data)) {
            echo json_encode("true");
        } else {
            echo json_encode("Wrong Password!");
        }
    }

    /**
     * Check whether current password is valid
     *
     * @param string $currentPassword
     * @return boolean
     */
    public function check_current_password($currentPassword) {
        $where_data = [
            'adminID' => $this->loginAdminID,
            'password' => hashString($currentPassword)
        ];
        $user_data = $this->CommonModel->get_row(TBL_ADMIN, 'adminID', $where_data);
        if (!empty($user_data)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_current_password', "Current Password is wrong.");
            return FALSE;
        }
    }
}
