<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public $loggedInUserID;

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('admin')) {
            redirect(site_url('admin/user'));
        }
	}

	public function index() {
		$data['title'] = SITE_NAME. ' | Admin Login';
		$this->template->load('admin_login_layout', 'admin/auth/index', $data);
	}

	public function logout() {
		$this->session->unset_userdata('admin');
		redirect(site_url());
	}

	public function doLogin() {
		try {

			if(!$this->input->post()) {
				throw new Exception("Invalid parameters");
			}

			$this->form_validation->set_rules('userName', 'userName', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'required');

			if(!$this->form_validation->run()) {
				throw new Exception(validation_errors());
			}

			$userData['userName'] = $this->input->post('userName');
			$userData['password'] = hashString($this->input->post('password'));
			
			$userSessionData = $this->CommonModel->get_row(TBL_ADMIN, '*', $userData);
			if(empty($userSessionData)) {
				throw new Exception("Invalid Login Credential!");
			}

			$this->session->set_userdata('admin', $userSessionData);

			$responseArray = [
				'code' => 100,
				'message' => "User has logged in successfully!",
			];
		} catch (Exception $e) {
			$responseArray = [
				'code' => 101,
				'message' => $e->getMessage()
			];
		}
		$this->set_response($responseArray);
	}

}
