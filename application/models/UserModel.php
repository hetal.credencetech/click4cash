<?php

class UserModel extends CI_Model
{

    public function getTransactionList($where = '1 = 1',$orderBy = 't.createdOn',$sortBy = 'DESC', $offset = '0', $limit = null, $rowCount = false){

        $this->db->select('t.transactionID,tu.phoneNumber,ba.bankName,ba.accountNumber,ba.accountName,t.rpTxnID,t.amount,t.status,t.remark,t.createdOn')
            ->from(TBL_TRANSACTION . ' t')
            ->join(TBL_BANK_ACCOUNT . ' ba', 't.bankAccountID = ba.bankAccountID')
            ->join(TBL_USER . ' tu', 'ba.userID = tu.userID')
            ->where('t.isDelete', 0)
            ->where('t.isActive', 1)
            ->where($where)
            ->order_by($orderBy, $sortBy);
            if($offset != 0 && $limit != null){
                $this->db->limit($limit, $offset);
            }

            if($rowCount) {
                return $this->db->count_all_results();
            } else {
                return $this->db->get()->result_array();
            }
    }
    public function getUserNotification($where = '1 = 1',$orderBy = 'tnd.notificationID',$sortBy = 'DESC'){

        $this->db->select('tn.title,tn.message,tn.createdOn,tn.notificationID')
            ->from(TBL_NOTIFICATION_DEVICE . ' tnd')
            ->join(TBL_NOTIFICATION . ' tn', 'tn.notificationID = tnd.notificationID AND tn.isDelete = 0 AND tn.isActive = 1')
            ->where('tnd.isDelete', 0)
            ->where('tnd.isActive', 1)
            ->where($where)
            ->order_by($orderBy, $sortBy);
        return $this->db->get()->result_array();
    }

    public function getAuthenticateUser($userID){

        $this->db->select('u.userID')
            ->from(TBL_USER . ' u')
            ->where('u.userID',$userID)
            ->where('u.isVerified',IsVerified::YES)
            ->where('u.isDelete', 0)
            ->where('u.isActive', 1)
            ->limit(1);
        return $this->db->get()->row_array();
    }

    public function getKycAuthenticateUser($userID){

        $this->db->select('u.userID')
            ->from(TBL_USER . ' u')
            ->join(TBL_USER_KYC . ' uk', 'uk.userID = u.userID AND uk.isDelete = 0 AND uk.isActive = 1 AND uk.status = '.KycStatus::APPROVED)
            ->where('u.userID',$userID)
            ->where('u.isVerified',IsVerified::YES)
            ->where('u.isDelete', 0)
            ->where('u.isActive', 1)
            ->limit(1);
        return $this->db->get()->row_array();
    }

    public function getKycList($columns = '*',$where = '1 = 1',$orderBy = 't.createdOn',$sortBy = 'DESC', $offset = '0', $limit = null, $rowCount = false){

        $this->db->select($columns)
            ->from(TBL_USER . ' u')
            ->join(TBL_USER_KYC . ' uk', 'uk.userID = u.userID AND uk.isDelete = 0 AND uk.isActive = 1')
            ->where('u.isVerified',IsVerified::YES)
            ->where('u.isDelete', 0)
            ->where('u.isActive', 1)
            ->where($where)
            ->order_by($orderBy, $sortBy);
            if($offset != 0 && $limit != null){
                $this->db->limit($limit, $offset);
            }

            if($rowCount) {
                return $this->db->count_all_results();
            } else {
                return $this->db->get()->result_array();
            }
    }


    public function getEnquiryList($columns = '*',$where = '1 = 1',$orderBy = 't.createdOn',$sortBy = 'DESC', $offset = '0', $limit = null, $rowCount = false){

        $this->db->select($columns)
            ->from(TBL_USER_ENQUIRY . ' ue')
            ->join(TBL_USER . ' u', 'u.userID = ue.userID AND u.isDelete = 0 AND u.isActive = 1','LEFT')
            ->where('ue.isDelete', 0)
            ->where('ue.isActive', 1)
            ->where($where)
            ->order_by($orderBy, $sortBy);
            if($offset != 0 && $limit != null){
                $this->db->limit($limit, $offset);
            }

            if($rowCount) {
                return $this->db->count_all_results();
            } else {
                return $this->db->get()->result_array();
            }
    }
}

?>