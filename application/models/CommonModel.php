<?php

class CommonModel extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function insert_batch($table_name, $data)
    {
        if ($this->db->insert_batch($table_name, $data) > 0) {
            return true;
        }
    }

    /**
     * inserts new record into table
     *
     * @param string $table
     * @param array $data
     * @return int last inserted ID
     */
    public function insert($table, $data) {
        $data = array_merge($data,['createdOn'=>getDefaultDate(),'updatedOn'=>getDefaultDate()]);
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    public function execRawQuery($query)
    {
        $query = $this->db->query($query);
        return $query->result_array();
    }
    /**
     * Get records of table
     *
     * @param string $table
     * @param string $columns
     * @param mixed $where
     * @param string $order_by
     * @param string $sort_by
     * @return array
     */
    public function get_all($table, $columns = '*', $where = '1 = 1', $order_by = null, $sort_by = 'DESC', $offset = '0',$limit = null) {
        $this->db->select($columns)
            ->from($table)
            ->where($where)
            ->where('isDelete', 0)
            ->where('isActive', 1);
        if($order_by != null) {
            $this->db->order_by($order_by, $sort_by);
        }
        if($limit != null) {
            $this->db->limit($limit);
        }

        $res = $this->db->get()->result_array();
        return $res;
    }

    /**
     * Set records as deleted
     *
     * @param string $table
     * @param mixed $where
     * @return int affected rows
     */
    public function set_delete($table, $where) {
        $this->db->where($where)
            ->update($table, ['isDelete' => 1]);
        return $this->db->affected_rows();
    }

    /**
     * update records
     *
     * @param string $table
     * @param mixed $data
     * @param mixed $where
     * @return int affected rows
     */
    public function update($table, $data, $where) {
        $this->db->set($data)
            ->where($where);
        // echo $this->db->get_compiled_update($table);exit;
        $this->db->update($table);
        return $this->db->affected_rows();
    }

    /**
     * Get single record for a table
     *
     * @param string $table
     * @param string $columns
     * @param string $where
     * @param string $order_by
     * @param string $sort_by
     * @return array
     */
    public function get_row($table, $columns = "*", $where = '1 = 1', $order_by = null, $sort_by = 'DESC') {
        $this->db->select($columns)
            ->from($table)
            ->where($where)           	
            ->where('isDelete', 0)	
            ->where('isActive', 1);

        $this->db->limit(1);
        if($order_by) {
            $this->db->order_by($order_by, $sort_by);
        }
        //return $this->db->get_compiled_select();
        return $this->db->get()->row_array();
    }

    public function get_num_rows($table, $where = "1 = 1") {
        $this->db->select('*')
            ->where('isActive', 1)
            ->where('isDelete', 0)
            ->where($where);
        return $this->db->count_all_results($table);
    }


    public function sendAndroidNotification($Message,$DeviceToken,$DeviceID){

        $curlData = json_encode(array('DeviceType' => $DeviceToken, 'DeviceID' => $DeviceID));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            'Authorization: key=' . GOOGLEAPIKEY,
            'Content-Type: application/json'
        );
        $fields = array(
            'to' => $DeviceToken,
            'notification' => $Message,
        );
        file_put_contents('AndroidReponse.txt',json_encode($fields));
        curl_setopt($ch, CURLOPT_URL, FCM_API_URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_PRIVATE, $curlData);

        //execute curl
        $result = curl_exec($ch);
        $curlData = json_decode(curl_getinfo($ch, CURLINFO_PRIVATE));
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // close
        curl_close($ch);
    }

    
    public function sendAppleNotification($Message,$DeviceToken,$DeviceID){

        $curlData = json_encode(array('DeviceType' => $DeviceToken, 'DeviceID' => $DeviceID));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $passphrase = '';       // Put your private key's passphrase here:
        if(IS_PRODUCTION)
        $PemFile = dirname(__FILE__) . '/../../uploads/notification_certificate/xyz.pem';
        else
        $PemFile = dirname(__FILE__) . '/../../uploads/notification_certificate/xyz.pem';
        $apnsTopic = 'innovativeiteration.Reward-Cards';
        $body = array();
        $body['aps'] = array(
            'alert' => $Message,
            'sound' => 'default',
            'content-available' => '1'
        );          // Create the payload body

        file_put_contents('iosReponse.txt',json_encode($body));
        $body = json_encode($body);
        
        if(IS_PRODUCTION)
        $url = IOS_APN_URL.$DeviceToken;
        else
        $url = IOS_APN_TEST_MODE_URL.$DeviceToken;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("apns-topic: $apnsTopic"));
        curl_setopt($ch, CURLOPT_SSLCERT, $PemFile);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $passphrase);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_PRIVATE, $curlData);

        //execute curl
        $result = curl_exec($ch);
        $curlData = json_decode(curl_getinfo($ch, CURLINFO_PRIVATE));
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // close
        curl_close($ch);
    }


}
?>