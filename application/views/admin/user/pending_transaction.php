<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><?= $page_heading ?></h4>
        </div>
    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Basic datatable -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <!-- <a href="<?=base_url();?>admin/user/add"
               class="btn btn-primary bg-teal-400 btn-labeled btn-labeled-left legitRipple"><b><i
                        class="icon-reading"></i></b> Add Customer</a> -->

            <!-- <a href="#" id="import_customer"
               class="btn btn-primary bg-teal-400 btn-labeled btn-labeled-left legitRipple"><b><i
                        class="icon-reading"></i></b> Import User</a> -->
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table" id="pendingTransactionDataTable">
                </table>
            </div>
        </div>
    </div>
    <!-- /basic datatable -->

</div>
<!-- /content area -->



<!-- Modal -->
<div id="TransactionStatusRemarkModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Transaction Remark</h4>
            </div>
            <div class="modal-body">
                <div class="">
                    <div id="FormMessage"></div>
                    <form id="TransactionStatusRemarkForm" action="" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="transactionID" id="transactionID" value="" />
                        <fieldset class="mb-3">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Status</label>
                                        <div class="col-lg-10">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label"><input type="radio" class="form-check-input status" name="status" value="1" /> Completed</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label"><input type="radio" class="form-check-input status" name="status" value="2" /> Failed</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Status Remark</label>
                                        <div class="col-lg-10">
                                            <textarea type="text" class="form-control" id="remark" name="remark"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="text-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary legitRipple">Save</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

