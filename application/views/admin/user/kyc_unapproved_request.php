<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><?= $page_heading ?></h4>
        </div>
    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Basic datatable -->
    <div class="card" >
        <div class="card-header header-elements-inline">
        </div>
        <div class="m-3" id="FormMessage"></div>
        <form id="UnapprovedKycStatusRemarkForm" action="" method="post" enctype="multipart/form-data">
                    
        <div class="card-body" id="kyc_unapproved_request">
            <input type="hidden" name="kycID" id="kycID" value="" />
            <input type="hidden" name="start" id="start" value="0" />
            <fieldset class="mb-3">
            <div class="form-group row">
                        <label class="col-form-label col-lg-2">Kyc ID</label>
                        <div class="col-lg-4">
                            <input type="kycID" name="kycID"  class="form-control" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Phone Number</label>
                        <div class="col-lg-4">
                            <input type="text" name="phoneNumber"  class="form-control" value="" readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Aadhar Front Image</label>
                        <div class="col-lg-4" id="aadharCardFrontImage">
                    
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Aadhar Back Image</label>
                        <div class="col-lg-4" id="aadharCardBackImage"> 
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">User Image</label>
                        <div class="col-lg-4" id="userImage">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Status</label>
                        <div class="col-lg-10">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label"><input type="radio" class="form-check-input status" name="status" value="1" /> Approved</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label"><input type="radio" class="form-check-input status" name="status" value="0" /> UnApproved</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Status Remark</label>
                        <div class="col-lg-10">
                            <textarea type="text" class="form-control" id="remark" name="remark"></textarea>
                        </div>
                    </div>

                    <div class="d-flex justify-content-end align-items-center">
                        <input id="prev" type="button" value="Previous"  class="btn btn-primary ml-3" style="display:none;"/> 
                        <input  type="submit" id="save_next" value="Save And Next"  class="btn btn-primary ml-3"/>
                    </div>
                </fieldset>
        </div> 
        </form>  
    </div>
    <!-- /basic datatable -->

</div>
<!-- /content area -->


