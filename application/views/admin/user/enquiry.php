<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><?= $page_heading ?></h4>
        </div>
    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Basic datatable -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <!-- <a href="<?=base_url();?>admin/user/add"
               class="btn btn-primary bg-teal-400 btn-labeled btn-labeled-left legitRipple"><b><i
                        class="icon-reading"></i></b> Add Customer</a> -->

            <!-- <a href="#" id="import_customer"
               class="btn btn-primary bg-teal-400 btn-labeled btn-labeled-left legitRipple"><b><i
                        class="icon-reading"></i></b> Import User</a> -->
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table" id="userEnquiryDataTable">
                </table>
            </div>
        </div>
    </div>
    <!-- /basic datatable -->

</div>
<!-- /content area -->
