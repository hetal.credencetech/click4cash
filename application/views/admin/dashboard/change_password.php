<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>Change Password</h4>
        </div>
    </div>
</div>

<div class="content">
    <div class="card">
        <div class="card-body">
            <div id="FormMessage"></div>            
            <form id="ChangePasswordForm" action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="newsPaperID" value="" />
                <fieldset class="mb-3">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Current Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" id="old_password" name="old_password" />
                                </div>
                            </div>                                                       
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">New Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" id="new_password" name="new_password" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Confirm Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" id="confirm_new_password" name="confirm_new_password" />
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="text-right">
                    <a href="<?= base_url('admin/dashboard') ?>" class="btn btn-default legitRipple">Cancel</a>
                    <button type="submit" class="btn btn-primary legitRipple">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>