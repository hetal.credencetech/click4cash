<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.interface.club/limitless/demo/bs4/Template/layout_1/LTR/material/full/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 07:18:05 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- <link rel="shortcut icon" href="<?= ASSETS_URL ?>images/ico/favicon.png"> -->
	<title><?= $title ?></title>

	<!-- Global stylesheets -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->
	<link href="<?= ADMIN_ASSETS_URL ?>global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>global_assets/css/icons/material/icons.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>global_assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/style.css" rel="stylesheet" type="text/css">

	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/main/jquery.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/ui/ripple.min.js"></script>

	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/pickers/daterangepicker.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/tables/datatables/extensions/fixed_columns.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/demo_pages/datatables_basic.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/demo_pages/extra_sweetalert.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/main/bootstrap-notify.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/forms/validation/validate.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/forms/validation/additional-methods.min.js"></script>
	<!-- <script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/editors/ckeditor/ckeditor.js"></script> -->
	<!-- <script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/forms/selects/select2.min.js"></script> -->

	<script src="<?= ASSETS_URL ?>js/select2.min.js"></script>

	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/demo_pages/form_inputs.js"></script>
	<!-- /theme JS files -->

	<!-- <script src="//cdn.ckeditor.com/4.11.4/full/ckeditor.js"></script> -->

	<!-- Theme JS files -->
	<script src="<?= ADMIN_ASSETS_URL ?>assets/js/app.js"></script>
	<!-- /theme JS files -->

	<script>
	var base_url = "<?= base_url() ?>";
	var site_url = "<?= site_url() ?>";
	</script>

</head>

<body class="">

	<!-- Main navbar -->

	<div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static">
		<div class="navbar-brand p-2">
			<a href="<?= site_url('admin/user') ?>" class="d-inline-block">
				<h6 class="header-title p-0 m-0">
				    <img src="<?= ASSETS_URL ?>img/click4cash.jpg" alt="" style ="width:132px; height:45px;" >
				</h6>
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item d-none">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-md-auto">
				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
						<span><?= $this->session->userdata('admin')['userName'] ?></span>
						<div class="legitRipple-ripple" style="left: 74.5481%; top: 60.4167%; transform: translate3d(-50%, -50%, 0px); width: 211.834%; opacity: 0;"></div>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="<?= site_url('admin/dashboard/change_password') ?>" class="dropdown-item"><i class="icon-user-plus"></i>Change Password</a>
						<div class="dropdown-divider"></div>
						<a href="<?= site_url('admin/logout') ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				<span class="font-weight-semibold">Navigation</span>
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->

			<!-- Sidebar content -->
			<div class="sidebar-content">
			    <?php
				$url_string = $this->uri->segment(2);
				if($this->uri->segment(3))
				$url_string .= "/".$this->uri->segment(3);
				?>
				<!-- Main navigation -->
				<?php $roleAccessList=$this->roleAccessList; ?>
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">
					<?php if(in_array("userMenu",$roleAccessList) || in_array("ALL",$roleAccessList)) :?>
						<li class="nav-item nav-item-submenu nav-item-expanded nav-item-open">
							<a href="#" class="nav-link legitRipple"><i class="icon-users" title="User"></i> <span>User</span></a>
							<ul class="nav nav-group-sub">
							    <?php if(in_array("user",$roleAccessList) || in_array("ALL",$roleAccessList)) :?>
								<li class="nav-item">
									<a href="<?= site_url('admin/user') ?>" class="nav-link <?=($this->router->class == 'user' && $this->router->fetch_method() == 'index'?'active':'')?>">
										<span>User List</span>
									</a>
								</li>
								<?php endif;?>

								<?php if(in_array("user",$roleAccessList) || in_array("ALL",$roleAccessList)) :?>
								<li class="nav-item">
									<a href="<?= site_url('admin/user/enquiry') ?>" class="nav-link <?=($this->router->class == 'user' && $this->router->fetch_method() == 'enquiry'?'active':'')?>">
										<span>User Enquiry List</span>
									</a>
								</li>
								<?php endif;?>

								<?php if(in_array("transaction",$roleAccessList) || in_array("ALL",$roleAccessList)) :?>
								<li class="nav-item">
									<a href="<?= site_url('admin/user/transaction') ?>" class="nav-link <?=($this->router->class == 'user' && $this->router->fetch_method() == 'transaction'?'active':'')?>">
										<span>Transaction List</span>
									</a>
								</li>
								<?php endif;?>


								<?php if(in_array("user",$roleAccessList) || in_array("ALL",$roleAccessList)) :?>
								<li class="nav-item">
									<a href="<?= site_url('admin/user/pending_transaction_list') ?>" class="nav-link <?=($this->router->class == 'user' && $this->router->fetch_method() == 'pending_transaction_list'?'active':'')?>">
										<span>Pending Transaction List</span>
									</a>
								</li>
								<?php endif;?>


								<?php if(in_array("user",$roleAccessList) || in_array("ALL",$roleAccessList)) :?>
								<li class="nav-item">
									<a href="<?= site_url('admin/user/kyc_request') ?>" class="nav-link <?=($this->router->class == 'user' && $this->router->fetch_method() == 'kyc_request'?'active':'')?>">
										<span>Kyc Request List</span>
									</a>
								</li>
								<?php endif;?>

								<?php if(in_array("user",$roleAccessList) || in_array("ALL",$roleAccessList)) :?>
								<li class="nav-item">
									<a href="<?= site_url('admin/user/kyc_unapproved_request') ?>" class="nav-link <?=($this->router->class == 'user' && $this->router->fetch_method() == 'kyc_unapproved_request'?'active':'')?>">
										<span>Kyc Unapproved Request</span>
									</a>
								</li>
								<?php endif;?>
				                
							</ul>
						</li>
						<?php endif;?>
					</ul>
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->

		</div>
		<!-- /main sidebar -->


		<!-- Main content -->
		<div class="content-wrapper">

			<?= $body ?>

			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; <?= date('Y') ?>. <?= SITE_NAME ?>
					</span>
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>

	<!-- View Data-->
	<div id="viewDataTableModal" class="modal fade" role="dialog" style="z-index: 999999 !important">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Record Detail</h4>
				</div>
				<div class="modal-body">
					<div>
						<table class="table table-condensed table-hover table-responsive">
							<tbody class="DataBody"></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	<!-- /page content -->
	<script src="<?= ADMIN_ASSETS_URL ?>assets/script/common.js?v=<?= FILE_VERSION ?>"></script>
	<?php echo isset($add_js)?$add_js:''; ?>

</body>

<!-- Mirrored from demo.interface.club/limitless/demo/bs4/Template/layout_1/LTR/material/full/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 07:35:18 GMT -->
</html>
