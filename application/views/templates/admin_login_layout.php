<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- <link rel="shortcut icon" href="<?= ASSETS_URL ?>images/ico/favicon.png"> -->
	<title><?= $title ?></title>

	<!-- Global stylesheets -->
    <!-- <link rel="shortcut icon" href="<?= ASSETS_URL ?>images/ico/favicon.png"> -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?= ADMIN_ASSETS_URL ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/main/jquery.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/ui/ripple.min.js"></script>
	<script src="<?= ADMIN_ASSETS_URL ?>global_assets/js/plugins/forms/validation/validate.min.js"></script>
	<!-- /core JS files -->

	<script>
		var baseUrl = "<?= site_url() ?>";
		var siteUrl = "<?= base_url() ?>";
	</script>

	<!-- Theme JS files -->
	<script src="<?= ADMIN_ASSETS_URL ?>assets/js/app.js"></script>
	<!-- /theme JS files -->
</head>
<body>
	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static">
		<div class="navbar-brand p-2">
			<a href="<?= site_url('admin') ?>" class="d-inline-block">
				<img src="<?= ASSETS_URL ?>img/click4cash.jpg" alt="" style ="width:132px; height:45px;">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
		</div>

	</div>
	<!-- /main navbar -->

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

            <?php echo $body; ?>

			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; <?= date('Y') ?>. <?= SITE_NAME ?>
					</span>
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

<script src="<?= ADMIN_ASSETS_URL ?>assets/script/auth.js"></script>
</body>
</html>