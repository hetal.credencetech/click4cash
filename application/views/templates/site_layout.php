<!DOCTYPE html>
<html lang="en">
<head>

	<meta http-equiv="content-type" content="text/html; charset=UTF-8">

	<title><?= $title ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">

	<!-- FAVICON -->
	<link rel="shortcut icon" href="<?= ASSETS_URL ?>images/favicon.ico">
	<link rel="stylesheet" href="<?= ASSETS_URL ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= ASSETS_URL ?>css/custom.css">

	<script>
	var site_url = "<?= site_url() ?>";
	var base_url = "<?= base_url() ?>";
	</script>
</head>
<body>
<div id="page-top"></div>

<div id="wrapper" class="wrapper">
<div id='cover-spin'></div>

    <!-- CONTENT -->
        <?php echo $body; ?>
    <!-- CONTENT -->
</div>
</body>
</html>
