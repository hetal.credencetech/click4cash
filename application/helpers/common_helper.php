<?php

/**
 * Prints array/object in pre-formatted
 *
 * @param array $array
 * @return void
 */
function p_r($var) {
    echo "<pre>";
    print_r($var);
    echo "<pre>";
}
/**
 * Prints array/object in pre-formatted
 *
 * @param array $array
 * @return void
 */
function pr($arr = null, $exit = 1, $append_text = null)
{
    if ($arr != null) {
        echo "<pre>";
        if ($arr != null)
            echo $append_text;

        print_r($arr);

        if ($exit == 1)
            exit;
        echo "</pre>";
    }
}


/**
 * Gets months with numbers
 *
 * @return array
 */
function month_array() {
    $months = [
        "1" => "Jan",
        "2" => "Feb",
        "3" => "Mar",
        "4" => "Apr",
        "5" => "May",
        "6" => "Jun",
        "7" => "Jul",
        "8" => "Aug",
        "9" => "Sep",
        "10" => "Oct",
        "11" => "Nov",
        "12" => "Dec",
    ];
    return $months;
}

function validateValue($value, $placeHolder)
{
    $value = strlen($value) > 0 ? $value : $placeHolder;
    return $value;
}
function getKeyValue($key, $placeHolder = '')
{
    $value = isset($_REQUEST[$key]) ? addslashes(trim($_REQUEST[$key])) : $placeHolder;
    return $value;
}
function validateObject($object, $key, $placeHolder)
{
    if (isset($object->$key)) {
        return $object->$key;
    } else {
        return $placeHolder;
    }
}

/**
 * hash string value
 *
 * @param string $string
 * @return string hashed string
 */
function hash_string($string) {
    return md5(PASSWORD_SALT.$string);
}

function json_validate($string)
{
    if (is_string($string)) {
        @json_decode($string);
        return (json_last_error() === JSON_ERROR_NONE);
    }
    return false;
}

function getDefaultDate()
{
    return date("Y-m-d H:i:s", time());
}

function convertDateTime($datetimeValue, $datetimeFormat = 'Y-m-d') {
    return date($datetimeFormat, strtotime($datetimeValue));
}

function matchStringValue($str1, $str2)
{
    if (strcmp($str1, $str2))
        return 1;
    else
        return 0;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function generateRandomCode($length = 4)
{
    $chars = "0123456789";
    return substr(ltrim(str_shuffle($chars), '0'), 0, $length);
}


/**
 * Hide actual phone number
 * @param string $phone
 * @return string
 */
function hidePhoneNumber($phone) {
    return substr_replace($phone,"*******",2, 6) ;
}


/**
 * Send sms to mobile number
 *
 * @param string $toMobile,
 * @param string $text
 * @return void
 */
function sendSMS($toMobile, $text) {
    $blockedPhoneNumberArray = [
        '9016240644'
    ];
    // global $blockedPhoneNumberArray;
    // pr($blockedPhoneNumberArray);exit;

    if(strlen($toMobile) != 10) {
        return false;
    }

    if(in_array($toMobile, $blockedPhoneNumberArray)) {
        return false;
    }

    $toMobile = "91".$toMobile;

    $params = [
        "apikey" => SMS_API_PASSWORD,
        "route" => SMS_API_USERNAME,
        "mobileno" => $toMobile,
        "sender" => SMS_API_SENDER_ID,
        "text"=> $text,
    ];

    $url = SMS_API_BASE_URL.http_build_query($params);
    return callCurl($url);
}

/**
 * Call curl
 *
 * @param string $url,
 * @return void
 */
function callCurl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $response = curl_exec($ch);
    curl_close($ch);

    $result = "[".date('Y-m-d H:i:s')."]\n".$response."\n\n";
    $log_file_name = "log_".date('Y-m-d').".txt";
    $log_file_path = APPPATH.'logs/'.$log_file_name;
    file_put_contents($log_file_path, $result, FILE_APPEND);

    return $response;
}

function callCurl2($url, $post_data = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);

    if($post_data) {
        curl_setopt($ch, CONNECTION_TIMEOUT, 0.1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
    }

    $response = curl_exec($ch);
    curl_close($ch);

    $result = "[".date('Y-m-d H:i:s')."]\n".$response."\n\n";
    $log_file_name = "log_".date('Y-m-d').".txt";
    $log_file_path = APPPATH.'logs/'.$log_file_name;
    file_put_contents($log_file_path, $result, FILE_APPEND);

    return $response;
}

function getFileUrlExtension($fileUrl) {
    $fileUrlArray = explode('.', $fileUrl);
    $fileUrlExtension = end($fileUrlArray);
    return $fileUrlExtension;
}
/**
 * hash string value
 *
 * @param string $string
 * @return string hashed string
 */
function hashString($string) {
    return md5(PASSWORD_SALT.$string);
}
?>
