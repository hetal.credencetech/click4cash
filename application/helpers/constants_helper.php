<?php
class StatusCodes
{
    const SUCCESS = 100;
    const FAILURE = 101;
    const ACCESS_DENIED = 105;
    const KYC_DENIED = 107;
}
class RoleEnum
{
    const ADMIN = 1;
    const USER = 2;
}
class TransactionStatus
{
    const ONHOLD = 0;
    const COMPLETED = 1;
    const FAILED = 2;
}
class DeviceType
{
    const ANDROID = 1;
    const IOS = 2;
}

class IsVerified
{
    const YES = 1;
    const NO = 0;
}

class KycUploaded
{
    const YES = 1;
    const NO = 0;
}

class KycStatus
{
    const NOTAPPROVE = 0;
    const APPROVED = 1;
}
?>