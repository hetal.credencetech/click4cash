<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
/*
| Custom Constants
*/

defined('SITE_NAME') OR define('SITE_NAME', 'Click 4 Cash');

$scheme = (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? "https" : "http";
$base_url = "";

if($_SERVER['HTTP_HOST'] == 'localhost') {
    $base_url = $scheme."://".$_SERVER['HTTP_HOST'].'/click4cash/';
} else if($_SERVER['HTTP_HOST'] == 'localhost:8080') {
    $base_url = $scheme."://".$_SERVER['HTTP_HOST'].'/click4cash/';
} else if($_SERVER['HTTP_HOST'] == 'credencetech.in') {
    $base_url = $scheme."://".$_SERVER['HTTP_HOST'].'/click4cash/';
} else {
    $base_url = $scheme."://".$_SERVER['HTTP_HOST'].'/';
}
defined('IS_PRODUCTION') OR define('IS_PRODUCTION', true);
defined('DOMAIN')	OR define('DOMAIN', trim($base_url, '/'));
defined('BASE_URL') OR define('BASE_URL', $base_url);

defined('DB_HOST')                  OR define('DB_HOST', 'localhost');
defined('DB_USERNAME')              OR define('DB_USERNAME', 'root');
defined('DB_PASSWORD')              OR define('DB_PASSWORD', '');
defined('DB_DATABASE')              OR define('DB_DATABASE', 'click4cash');

defined('UPLOAD_URL')     OR define('UPLOAD_URL', BASE_URL.'uploads/');
defined('UPLOAD_PATH')     OR define('UPLOAD_PATH', FCPATH.'uploads/');

defined('KYC_FRONT_UPLOAD_PATH')     OR define('KYC_FRONT_UPLOAD_PATH', 'kyc/aadhar_card_front_image/');
defined('KYC_BACK_UPLOAD_PATH')     OR define('KYC_BACK_UPLOAD_PATH', 'kyc/aadhar_card_back_image/');
defined('USER_IMAGE_UPLOAD_PATH')     OR define('USER_IMAGE_UPLOAD_PATH', 'kyc/user_image/');
defined('ATTACHMENT_UPLOAD_PATH')     OR define('ATTACHMENT_UPLOAD_PATH', 'attachment/');

defined('PASSWORD_SALT') OR define('PASSWORD_SALT', '3g5fh4bsdo');
/*
|Tables
*/
defined('TBL_USER') OR define('TBL_USER', "tbluser");
defined('TBL_ADMIN') OR define('TBL_ADMIN', "tbladmin");
defined('TBL_BANK_ACCOUNT') OR define('TBL_BANK_ACCOUNT', "tblbankaccount");
defined('TBL_CONFIG') OR define('TBL_CONFIG', "tblconfig");
defined('TBL_PURPOSE') OR define('TBL_PURPOSE', "tblpurpose");
defined('TBL_NOTIFICATION') OR define('TBL_NOTIFICATION', "tblnotification");
defined('TBL_NOTIFICATION_DEVICE') OR define('TBL_NOTIFICATION_DEVICE', "tblnotificationdevice");
defined('TBL_TRANSACTION') OR define('TBL_TRANSACTION', "tbltransaction");
defined('TBL_USER_KYC') OR define('TBL_USER_KYC', "tbluserkyc");
defined('TBL_ADMIN_ROLE') OR define('TBL_ADMIN_ROLE', "tbladminrole");
defined('TBL_USER_ENQUIRY') OR define('TBL_USER_ENQUIRY', "tbluserenquiry");

/*
| Messages for App Response
*/
defined('ERROR_MESSAGE') or define("ERROR_MESSAGE","Something Went wrong. Please try again Later.");
defined('ACCESS_DENIED') or define("ACCESS_DENIED","Access Denied");
defined('JWT_KEY') OR define('JWT_KEY', 'click4cash');


/*SMS Details*/
defined('SMS_API_BASE_URL') OR define('SMS_API_BASE_URL', 'http://bulksms.credencetech.in/api/push?');
defined('SMS_API_USERNAME') OR define('SMS_API_USERNAME', 'trans_dnd');
defined('SMS_API_PASSWORD') OR define('SMS_API_PASSWORD', '5e4f774a86d5b');
defined('SMS_API_SENDER_ID') OR define('SMS_API_SENDER_ID', 'MILKOG');

defined('RAZORPAY_KEY_ID') OR define('RAZORPAY_KEY_ID', 'rzp_test_pP5HrR4WGQPpZI');
defined('RAZORPAY_KEY_SECRET') OR define('RAZORPAY_KEY_SECRET', 'lsDXFMTfyqJIjmpzX6jt3yzb');


//push notification configuration
defined('SANDBOX_MODE') OR define('SANDBOX_MODE', true);
defined('FCM_API_URL') OR define('FCM_API_URL', 'https://fcm.googleapis.com/fcm/send');
defined('GOOGLEAPIKEY') OR define('GOOGLEAPIKEY', '');
defined('IOS_APN_URL') OR define('IOS_APN_URL', 'https://api.push.apple.com/3/device/');
defined('IOS_APN_TEST_MODE_URL') OR define('IOS_APN_TEST_MODE_URL', 'https://api.sandbox.push.apple.com/3/device/');
defined('ANDROID_BATCH_COUNT') OR define('ANDROID_BATCH_COUNT', 1);
defined('ANDROID_DEVICE_TOKEN_LENGTH') OR define('ANDROID_DEVICE_TOKEN_LENGTH', 152);

defined('ASSETS_URL')                                       OR define('ASSETS_URL', BASE_URL.'assets/');
defined('ADMIN_ASSETS_URL')                                 OR define('ADMIN_ASSETS_URL', BASE_URL.'admin_assets/');
defined('FILE_VERSION')                                 OR define('FILE_VERSION', 1);
