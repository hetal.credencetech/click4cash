### Installation
- Edit below configuration files as per your development environment.
    - application/config/constants.php
- Execute `composer install` at root directory to install third party libraries.
- Create uploads folder and following directory Structure
    --|uploads
       --|items
